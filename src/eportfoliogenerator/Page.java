/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.json.JsonArray;
import javax.json.JsonValue;

/**
 *
 * @author Scott
 */
public class Page extends VBox{

   
    
    EPortfolioGeneratorView ui;
        ObservableList<Component> components;
    PageEditView pageEdit;
    ListView<String> listView= new ListView();
   List<String> list;
         List<String> links;
    String title="";
    String footer="";
    String x="";
    String total="";
    String studentName="";
    String layout="";
    String color="";
    String font="";
    String bannerImage;
    String fileName="";
    String locationx="";
    
    String bannerImageName;
  SlideShowFileManager ssfm = new SlideShowFileManager();
    SlideShowMakerView ssmv = new SlideShowMakerView(ssfm);
     SlideShowModel slideShow= new SlideShowModel(ssmv);
   Component  selectedComponent;
   ObservableList<ComponentEditView> componentEditors;
    private Object label;
    public Page(String title, EPortfolioGeneratorView initUI){
        ui = initUI;
         componentEditors = FXCollections.observableArrayList();
        components = FXCollections.observableArrayList();
        this.title=title;
    }
    public String getBannerImageName(){
        return bannerImageName;
    }
    public SlideShowModel getSlideShow(){
        return slideShow;
    }
    public void setSlideShow(SlideShowModel slideShow){
        this.slideShow = slideShow;
    }
    public void setBannerImageName(String bannerImageName){
        this.bannerImageName = bannerImageName;
    }
    public void setTitle(String title){
        this.title=title; 
    }
    public String getTitle(){
         return title;
     }
    public void setBannerImage(String bannerImage){
       this.bannerImage = bannerImage;
    }
    public String getBannerImage(){
        return bannerImage;
    }
    public void setFooter(String footer){
        this.footer=footer;
    }
    public void setStudentName(String studentName){
        this.studentName= studentName;
    }
    public String getFooter(){
        return footer;
    }
    public String getStudentName(){
        return studentName;
        
    }
    
    public void setLayout(String layout){
        this.layout=layout;
        
    }
    public void setColor(String color){
        this.color=color;
    }
    public void setFont(String font){
        this.font=font;
    }
    public String getColor(){
        return color;
    }
    public String getLayout(){
        return layout;
    }
    public String getFont(){
        return font;
    }
  public void clear(){
      components.clear();
  }
     public void removeComponent( ) {
	components.remove(getSelectedComponent());
	ui.reloadPagePane(this);
    }
      public void editComponent( ) {
          Component component;
	component=getSelectedComponent();
        if(component.getType().equals("header")){
            Stage stage = new Stage();
                   VBox pane = new VBox();
                   
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   Label height = new Label("height in px:");
                   TextField heightField = new TextField();
                   HBox heightBox = new HBox();
                   heightBox.getChildren().addAll(height, heightField);
                   Label width = new Label("width in px:");
                   TextField widthField = new TextField();
                   HBox widthBox = new HBox();
                   widthBox.getChildren().addAll(width, widthField);
                   Label caption = new Label("Header:  ");
                   TextField captionField = new TextField(getSelectedComponent().getText());
                   HBox captionBox = new HBox();
                   captionBox.getChildren().addAll(caption, captionField);
                   
                   HBox Hbox = new HBox();
                   Button file = new Button("Choose File");
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll( Ok, canc);
                   pane.getChildren().addAll( captionBox, Hbox);
                   pane.setSpacing(12);
                   
             Ok.setOnAction(e->{
                Page pagem = ui.getPage();
                 int y = components.indexOf(getSelectedComponent());
                components.remove(getSelectedComponent());
                String x = captionField.getText();
                 pagem.addComponent(null, null, "header", null, x, null, null, null, null, null, null, null,null,null,y);
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   
        }
        if(component.getType().equals("image")) {
           this.ui=ui;
              Stage stage = new Stage();
            
                   VBox pane = new VBox();
                   Label locationLabel = new Label("location:  ");
                   ChoiceBox location = new ChoiceBox();
                   location.getItems().addAll("left", "right", "middle");
                   location.getSelectionModel().selectFirst();
                   HBox loc = new HBox();
                    location.getSelectionModel().selectedItemProperty()
            .addListener(e -> {
           locationx= (String)location.getSelectionModel().getSelectedItem(); 
            });
                   loc.getChildren().addAll(locationLabel, location);
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   Label height = new Label("height in px:   ");
                   TextField heightField = new TextField(component.getHeight());
                   HBox heightBox = new HBox();
                   heightBox.getChildren().addAll(height, heightField);
                   Label width = new Label("width in px:   ");
                   TextField widthField = new TextField(component.getWidth());
                   HBox widthBox = new HBox();
                   widthBox.getChildren().addAll(width, widthField);
                   Label caption = new Label("caption:        ");
                   TextField captionField = new TextField(component.getCaption());
                   HBox captionBox = new HBox();
                   captionBox.getChildren().addAll(caption, captionField);
                   
                   HBox Hbox = new HBox();
                   Button file = new Button("Choose File");
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll(file, Ok, canc);
                   pane.setSpacing(12);
                   pane.getChildren().addAll(loc, heightBox, widthBox, captionBox, Hbox);
                   fileName=component.getFile();
                   
                       file.setOnAction(e->{
                       FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
     
              
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
        File filez = imageFileChooser.showOpenDialog(null);
          String path = filez.getPath().substring(0, filez.getPath().indexOf(filez.getName()));
	     fileName = filez.getName();
          //  component.setInItFile(path+fileName);
	File filem = new File(path+fileName);
       
          BufferedImage image = null;
          Page pagem = ui.getPage();
          int x= ui.getEPortfolio().getPages().indexOf(pagem);
          System.out.println("page number" + x);
          String num="";
          if(x==0){
              
          }
          else{
         num = x +1 + "";
          }
           new File("./Sites/" + ui.getTitle()+ "/index" + num).mkdirs();
        try {
          
            
            image = ImageIO.read(filem);
            
            ImageIO.write(image, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/" + fileName + ".jpg"));
            ImageIO.write(image, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/" + fileName + ".jpg"));
            ImageIO.write(image, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/" + fileName + ".jpg"));
            
        } catch (IOException k) {
        	k.printStackTrace();
        }
           
        
      
                   });
             Ok.setOnAction(e->{
                  Page pagem = ui.getPage();
                   int x = components.indexOf(getSelectedComponent());
                     components.remove(getSelectedComponent());
                   pagem.addComponent(null, fileName, "image", captionField.getText(), null, heightField.getText(), widthField.getText(), null, null, locationx, null, null,null, null,x);
                stage.close(); 
                stage.close(); 
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   

       }
        if(component.getType().equals("video")) {
              Stage stage = new Stage();
                   VBox pane = new VBox();
                   
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   Label height = new Label("height in px:    ");
                   TextField heightField = new TextField(component.getHeight());
                   HBox heightBox = new HBox();
                   heightBox.getChildren().addAll(height, heightField);
                   Label width = new Label("width in px:    ");
                   TextField widthField = new TextField(component.getWidth());
                   HBox widthBox = new HBox();
                   widthBox.getChildren().addAll(width, widthField);
                   Label caption = new Label("caption:       ");
                   TextField captionField = new TextField(component.getCaption());
                   HBox captionBox = new HBox();
                   captionBox.getChildren().addAll(caption, captionField);
                   
                   HBox Hbox = new HBox();
                   Button file = new Button("Choose File");
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll(file, Ok, canc);
                   pane.setSpacing(12);
                   pane.getChildren().addAll(heightBox, widthBox, captionBox, Hbox);
                   fileName=component.getVideo();
                               file.setOnAction(e->{
                       FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
     
              
	FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	
        imageFileChooser.getExtensionFilters().addAll(mp4Filter);
        File filez = imageFileChooser.showOpenDialog(null);
          String path = filez.getPath().substring(0, filez.getPath().indexOf(filez.getName()));
	     fileName = filez.getName();
          //  component.setInItFile(path+fileName);
	File filem = new File(path+fileName);
       
          BufferedImage image = null;
          Page pagem = ui.getPage();
          int x= ui.getEPortfolio().getPages().indexOf(pagem);
          System.out.println("page number" + x);
          String num="";
          if(x==0){
              
          }
          else{
         num = x +1 + "";
          }
           new File("./Sites/" + ui.getTitle()+ "/index" + num).mkdirs();
        File vidDest = new File( "./Sites/" + ui.getTitle() + "/index" + num + "/" + fileName + ".mp4");
   
     
       
try{
Files.copy(filem.toPath(), vidDest.toPath());
} catch(IOException k){
}
});
             Ok.setOnAction(e->{
                   Page pagem = ui.getPage();
                    int y = components.indexOf(getSelectedComponent());
                    System.out.println(" comp" + y);
                 components.remove(getSelectedComponent());
                 pagem.addComponent(null, null, "video", captionField.getText(), null, heightField.getText(), widthField.getText(), null, null, null, null, null,fileName, null,y);
                stage.close(); 
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   

       }
        if(component.getType().equals("slideshow")) {
            SlideShowFileManager fileManager = new SlideShowFileManager();
 SlideShowMakerView view = new SlideShowMakerView(fileManager);

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
            VBox pane = new VBox();
                   Stage stage =new Stage();
                
                    
                    
                  view.startUI(stage, "SlideShowMaker");
                     view.getSlideShow().setTitle(component.getSlideShow().getTitle());
                 
                   
                    view.getSlideShow().setSlides(component.getSlideShow().getSlides()); 
                 view.reloadSlideShowPane(component.getSlideShow());
                      view.getSlideShow().setTitle(component.getSlideShow().getTitle());
                
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   HBox Hbox = new HBox();
                   Hbox.getChildren().addAll( Ok, canc);
                   
   


                  
                   
           view.OKButton.setOnAction(e->{
                Page pagem = ui.getPage();
                int x = components.indexOf(getSelectedComponent());
                   components.remove(getSelectedComponent());
                      int i=0; slideShow.getSlides().size();
               while(i<slideShow.getSlides().size()){
                    fileName = slideShow.getSlides().get(i).getImageFileName();
                File filem = new File(slideShow.getSlides().get(i).getImagePath()+fileName);
       
          BufferedImage image = null;
          
          int l= ui.getEPortfolio().getPages().indexOf(pagem);
          System.out.println("page number" + l);
          String num="";
          if(l==0){
              
          }
          else{
         num = l +1 + "";
          }
           new File("./Sites/" + ui.getTitle()+ "/index" + num).mkdirs();
        try {
          
            
            image = ImageIO.read(filem);
            
            ImageIO.write(image, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/" + fileName + ".jpg"));
            ImageIO.write(image, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/" + fileName + ".jpg"));
            ImageIO.write(image, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/" + fileName + ".jpg"));
            
        } catch (IOException k) {
        	k.printStackTrace();
        }
           
             i++;   }
               try {
                     int l= ui.getEPortfolio().getPages().indexOf(pagem);
          System.out.println("page number" + x);
          String num="";
          if(l==0){
              
          }
          else{
         num = l +1 + "";
          }
          
            File fileA = new File("images/icons/Next.png");
            BufferedImage imageA = null;
            imageA = ImageIO.read(fileA);
            
            ImageIO.write(imageA, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/next.jpg"));
            ImageIO.write(imageA, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/next.jpg"));
            ImageIO.write(imageA, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/next.jpg"));
            
                File fileB = new File("images/icons/prev.png");
            BufferedImage imageB = null;
            imageB = ImageIO.read(fileB);
            
            ImageIO.write(imageB, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/prev.jpg"));
            ImageIO.write(imageB, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/prev.jpg"));
            ImageIO.write(imageB, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/prev.jpg"));
            
                File fileC = new File("images/icons/pause.png");
            BufferedImage imageC = null;
            imageC = ImageIO.read(fileC);
            
            ImageIO.write(imageC, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/pause.jpg"));
            ImageIO.write(imageC, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/pause.jpg"));
            ImageIO.write(imageC, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/pause.jpg"));
            
                   File fileD = new File("images/icons/play.png");
            BufferedImage imageD = null;
            imageD = ImageIO.read(fileD);
            
            ImageIO.write(imageD, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/play.jpg"));
            ImageIO.write(imageD, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/play.jpg"));
            ImageIO.write(imageD, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/play.jpg"));
        } catch (IOException k) {
        	k.printStackTrace();
        }
      
                 pagem.addComponent(null, null, "slideshow", null, null, null, null, null, null, null, null, null,null, view.getSlideShow(),x);
                stage.close(); 
             });     
           view.CancelButton.setOnAction(e->{  
                stage.close(); 
                
           });
        }
        if(component.getType().equals("list")) {
              Stage stage = new Stage();
                   VBox pane = new VBox();
                   
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   listView = new ListView<>();
                   
                   listView.getItems().addAll(component.getListview().getItems());
              
                   HBox Hbox = new HBox();
           
                   Button Ok = new Button("Ok");
                   Button remove = new Button("Remove");
                   Button Add = new Button("Add");
                 
                   TextField textField = new TextField();
                     HBox more = new HBox();
                   more.getChildren().addAll(Add, textField);
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll( remove, Ok, canc);
                   pane.getChildren().addAll( listView, more, Hbox);
                   remove.setOnAction(e->{
                       String select = listView.getSelectionModel().getSelectedItem();
                       
                       for(int i=0; i< listView.getItems().size(); i++ ){
                           if(select.equals(listView.getItems().get(i))){
                           listView.getItems().remove(i);
                       }
                       }
                   });
                   
                   Add.setOnAction(e->{
                       if(listView.getSelectionModel().getSelectedItem()==null){
                      listView.getItems().addAll(textField.getText());
                      textField.clear();
                       }
                       else{
                          
                           String select = listView.getSelectionModel().getSelectedItem();
                       
                       for(int i=0; i< listView.getItems().size(); i++ ){
                           if(select.equals(listView.getItems().get(i))){
                               
                          listView.getItems().add(i+1, textField.getText());
                               textField.clear();
                               break;
                       }
                       }
                       }
                      
                      
                   });
             Ok.setOnAction(e->{
                 Page pagem = ui.getPage();
                  int x = components.indexOf(getSelectedComponent());
                 components.remove(getSelectedComponent());
                  pagem.addComponent(null, null, "list", null, null, null, null, null, null, null, null, listView,null, null,x);
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
        }
        if(component.getType().equals("paragraph")) {
    Stage stage = new Stage();
                   VBox pane = new VBox();
                   ArrayList list = new ArrayList();
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   Label height = new Label("height in px:");
                   TextField heightField = new TextField("100");
                   HBox heightBox = new HBox();
                   heightBox.getChildren().addAll(height, heightField);
                   Label width = new Label("width in px:");
                   TextField widthField = new TextField("50");
                   HBox widthBox = new HBox();
                   widthBox.getChildren().addAll(width, widthField);
                   Label caption = new Label("paragraph:  ");
                   TextArea captionField = new TextArea(component.getStrings().get(component.getStrings().size()-1));
                   
                   HBox captionBox = new HBox();
                   captionBox.getChildren().addAll(caption, captionField);
                        ChoiceBox font = new ChoiceBox();
 font.getItems().addAll("Open Sans", "Montserrat", "Lora", "Ubuntu", "Oxygen");
// font.getSelectionModel().setSelectedItem();
 Label fontLabel = new Label("font:  ");
 HBox fontBox = new HBox();
 fontBox.getChildren().addAll(fontLabel, font);
                   HBox Hbox = new HBox();
                   int i=0;
                   Button file = new Button("Choose File");
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll( Ok, canc);
                   pane.getChildren().addAll( fontBox, captionBox);
                    captionField.setEditable(true);
        Button selectHype = new Button ("Select Text");
        Button removeHype = new Button ("remove");
        HBox Box = new HBox();
        Box.getChildren().addAll(selectHype, removeHype);
        HBox Hbox2 = new HBox();
   
        Label link = new Label ("link"); 
        TextField area = new TextField();
          Hbox2.getChildren().addAll(link, area, Box);
	pane.getChildren().addAll(Hbox2, Hbox);
        
      total = captionField.getText();
     removeHype.setDisable(true);
         if(component.getHyperLink()!=null){
              removeHype.setDisable(false);
     while(i<component.getHyperLink().size()){
      
               Label sample = new Label("Select Text: " +   component.getHyperlinkText().get(i));
        System.out.println(sample);
        Label site = new Label ("Link: " + component.getHyperLink().get(i));
      
       HBox Vbox = new HBox();
        Vbox.getChildren().addAll(sample, site);
      Box.getChildren().addAll(Vbox);
     i++;
     }
     
         }
         
        selectHype.setOnAction (e->{ 
            
            System.out.println("m");
            component.getStrings().add(captionField.getText());
               Label sample = new Label("Select Text: " +   captionField.getSelectedText());
               component.getHyperlinkText().add(captionField.getSelectedText());
        String linkSite = area.getText(); 
        removeHype.setDisable(false);
        Label site = new Label ("Link: " + linkSite);
        component.getHyperLink().add(linkSite);
        HBox Vbox = new HBox();
        Vbox.getChildren().addAll(sample, site);
       
            Box.getChildren().addAll(Vbox);
            int x = captionField.getText().indexOf(captionField.getSelectedText());
            String m = captionField.getText().substring(0, x);
            String y = captionField.getText().substring(x ,captionField.getSelectedText().length()+x);
            String z = captionField.getText().substring(captionField.getSelectedText().length()+x);
             total = m + "<a href=\"" + linkSite + "\">" + y + "</a>" + z;
             System.out.print( " total " + total);
             captionField.setText(total);
            
         
        });
        removeHype.setOnAction(e->{
            component.getStrings().add(captionField.getText());
           // System.out.println( " string 1" + strings.get(strings.size()-1));
            if(component.getStrings().get(component.getStrings().size()-1).equals(component.getStrings().get(component.getStrings().size()-2))){
            component.getStrings().remove(component.getStrings().size()-1);
            component.getStrings().remove(component.getStrings().size()-1);
            }
            else{
                component.getStrings().remove(component.getStrings().size()-1);
            }
            //System.out.println(strings.get(strings.size()-1));
            captionField.setText(component.getStrings().get(component.getStrings().size()-1));
            component.getHyperLink().remove(component.getHyperLink().size()-1);
             component.getHyperlinkText().remove(component.getHyperlinkText().size()-1);
           Box.getChildren().remove(Box.getChildren().size()-1);
           if(component.getHyperLink().size()<1){
                  removeHype.setDisable(true);
           }
           
        });
         font.getSelectionModel().selectedItemProperty()
            .addListener(e -> {
            x= (String)font.getSelectionModel().getSelectedItem(); 
            });
             Ok.setOnAction(e->{
                 if(total==null){
                     total = captionField.getText();
                 }
                   Page pagem = ui.getPage();
                          component.getStrings().remove(component.getStrings().size()-1);
                    total = captionField.getText();
               component.getStrings().add(total);
                 int y = components.indexOf(getSelectedComponent());
                   components.remove(getSelectedComponent());
                 pagem.addComponent(component.getStrings(), null, "paragraph", null, component.getStrings().get(component.getStrings().size()-1), null, null, component.getHyperlinkText(), component.getHyperLink(), null, x, null,null, null,y);
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   

       }
                
	ui.reloadPagePane(this);
    }
     public void setPageEdit(PageEditView pageEdit){
        this.pageEdit=pageEdit;
    }
     
     public void addComponent(List<String> strings, String file, String type, String caption, String text, String height, String width, List<String> hyperlinkText, List<String> hyperLink, String location, String font, ListView listview,String video, SlideShowModel slideShow){
         
             Component component = new Component(strings, file, type, caption, text, height, width,  hyperlinkText,  hyperLink, location, font, listview,video, slideShow);
         if(getSelectedComponent()!=null){
             int x = components.indexOf(getSelectedComponent());
             components.add(x+1, component);
         }
         else{
         components.add(component);
         }
         System.out.println(components.size());
       selectedComponent=null;
         ui.reloadPagePane(this);
     }
       public void addComp(JsonArray strings, String file, String type, String caption, String text, String height, String width, JsonArray hyperlinkText, JsonArray hyperLink, String location, String font, JsonArray listview,String video, JsonArray slideShow, String slideShowTitle){
      
           List<String> list1 = new ArrayList<String>();
       
for (int i=0; i<strings.size(); i++) {
    list1.add( strings.getString(i) );
}
 List<String> list2 = new ArrayList<String>();
for (int i=0; i<hyperlinkText.size(); i++) {
    list2.add( hyperlinkText.getString(i) );
}
 List<String> list3 = new ArrayList<String>();
for (int i=0; i<hyperLink.size(); i++) {
    list3.add( hyperLink.getString(i) );
}
 ListView<String> list4 = new ListView();
for (int i=0; i<listview.size(); i++) {
    list4.getItems().add( listview.getString(i) );
}

           addComponent(list1, file, type, caption, text, height, width, list2, list3, location, font, list4, video, null);
       
       }
      
     public void addComponent( List<String> strings, String file, String type, String caption, String text, String height, String width, List<String> hyperlinkText, List<String> hyperLink, String location, String font, ListView listview,String video, SlideShowModel slideShow, int y){
         
             Component component = new Component(strings, file, type, caption, text, height, width, hyperlinkText,  hyperLink, location, font, listview,video, slideShow);
             
        
             
         components.add(y,  component);
         
         System.out.println(components.size());
       selectedComponent=null;
         ui.reloadPagePane(this);
     }
      public ObservableList<Component> getComponents() {
	return components;
    }
       public void setComponents(ObservableList<Component> components) {
	this.components= components;
    }
    
    public Component getSelectedComponent() {
	return selectedComponent;
    }
   public void setSelectedComponent(Component component){
       selectedComponent=component;
   }
   public void selectComponent(){
       ui.reloadPagePane(this);
   }

    /**
     * @return the list
     */
    public List<String> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List<String> list) {
        this.list = list;
    }

    /**
     * @return the links
     */
    public List<String> getLinks() {
        return links;
    }

    /**
     * @param links the links to set
     */
    public void setLinks(List<String> links) {
        this.links = links;
    }

    

   

   

    
    
 
     
}
