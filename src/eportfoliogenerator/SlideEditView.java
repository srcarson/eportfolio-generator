package eportfoliogenerator;

import java.io.File;
import java.net.URL;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class SlideEditView extends HBox {
    private SlideShowMakerView slideShow;
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    boolean selected;
    private Button btSubmit;
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    String captionText="String";
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add("slide_edit_view");
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	
	captionLabel = new Label("caption");
         //btSubmit = new Button("Submit");
	captionTextField = new TextField(slide.getCaption());
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);
        //captionVBox.getChildren().add(btSubmit);
      
	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
        captionTextField.setOnKeyReleased(e->{
            
            slide.setCaption(captionTextField.getText());
        });
	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
            
	    imageController.processSelectImage(slide, this);
	});
        
    }
    public SlideEditView getSlideEditView(){
        return this;
    }
    public Slide getSlide(){
        return slide;
    }
  public void setSlide(Slide slide){
      this.slide=slide;
  }
  public void setSelected(){
      this.selected=true;
  }
  
  public void setDeSelected(){
      this.selected=false;
  }
  public boolean isSelected(){
      if (this.selected==true){
          return true;
      }
      else{
          return false;
      }
  }
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	try {
        String imagePath = slide.getImagePath() + "/" + slide.getImageFileName();
	File file = new File(imagePath);
	
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
               if(slideImage.getWidth()==0){
         
	String missing = "missing";
                               imagePath = "slide_show_images" + "/" + "DefaultStartSlide.png";
                               Label label  = new Label(slide.getImageFileName() + " " + missing);
	 file = new File(imagePath);
         slide.setImagePath("slide_show_images" + "/");
         slide.setImageFileName("DefaultStartSlide.png");
	
	    // GET AND SET THE IMAGE
         
	     fileURL = file.toURI().toURL();
	     slideImage = new Image(fileURL.toExternalForm());
             VBox pane = new VBox();
             Scene scene = new Scene(pane);
             Stage stage = new Stage();
             stage.setTitle(missing);
             Button button = new Button("OK");
             //pane.getChildren().add();
             //pane.getChildren().add();
             pane.getChildren().add(button);
             pane.getChildren().add(label);
              button.setOnMouseClicked(e->{
                 stage.close();
             });
             stage.setScene(scene);
      
             
             stage.showAndWait();
              
            }
        
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = 200;
            
	    double perc = scaledWidth / slideImage.getWidth();
         
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
           
	    // @todo - use Error handler to respond to missing image
	}
    }    
}