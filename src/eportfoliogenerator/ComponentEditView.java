/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 *
 * @author Scott
 */
public class ComponentEditView extends HBox{
        private Page page;
    // SLIDE THIS COMPONENT EDITS
    Component component;
    boolean selected;
    private Button btSubmit;
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public ComponentEditView(Component component)  {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add("component_edit_view");
	
	// KEEP THE SLIDE FOR LATER
	this.component = component;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	
	if(component.getType().equals("header")){
         //btSubmit = new Button("Submit");
            Label label2 = new Label("header");
	Label label = new Label (component.getText());
	getChildren().addAll(label2,label);
        
      
    }
        if(component.getType().equals("slideshow")){
         //btSubmit = new Button("Submit");
            Label label2 = new Label("slideshow:");
          SlideShowModel slideshow = component.getSlideShow();
          
	Label label = new Label (component.getSlideShow().getTitle());
	getChildren().addAll(label2,label);
        
      
    }
        if(component.getType().equals("list")){
         //btSubmit = new Button("Submit");
            Label label2 = new Label("list");
            List<String> list = component.getListview().getItems();
           int i=0;
           String all="";
           while(i<list.size()){
             all += list.get(i) + "\n";
                    i++;}
	Label label = new Label (all);
	getChildren().addAll(label2,label);
        
      
    }
        if(component.getType().equals("paragraph")){
         //btSubmit = new Button("Submit");
            Label label2 = new Label("page");
            Label label3 = new Label ("font: " + component.getFont());
	TextArea label = new TextArea (component.getStrings().get(component.getStrings().size()-1));
        label.setEditable(false);
        label.getStyleClass().add("width: 20px");
        label.getStyleClass().add("-fx-font-size: 10");
        
        HBox Hbox = new HBox();
      VBox  vbox = new VBox();
      
   
        VBox v = new VBox();
        
    
          Hbox.getChildren().addAll(v, vbox);
	getChildren().addAll(label2,label,Hbox, label3);
      int i=0;
      System.out.println(" comp o " + component.getHyperLink());
      if(component.getHyperLink()!=null){
     while(i<component.getHyperLink().size()){
      
               Label sample = new Label("Select Text: " +   component.getHyperlinkText().get(i));
        System.out.println(sample);
        Label site = new Label ("Link: " + component.getHyperLink().get(i));
      
       
        v.getChildren().addAll(sample, site);
     
     i++;
     }
      }
        
        
        
      
    }
        
        if(component.getType().equals("video")){
         //btSubmit = new Button("Submit");
            Label label2 = new Label("video");
            Label label3 = new Label ("caption: " + component.getCaption());
            Label label = new Label("Height: "+ component.getHeight() + "px");
            Label label4 = new Label("width " + component.getWidth() + "px");
  
    
	getChildren().addAll(label2,label3, label, label4 );
      
     
        
      
        }
         if(component.getType().equals("image")){
         //btSubmit = new Button("Submit");
            Label label2 = new Label("image");
            Label label3 = new Label ("caption: " + component.getCaption());
            Label label = new Label("Height: " + component.getHeight() + "px");
            Label label4 = new Label("width: " + component.getWidth() + "px");
            Label label5 = new Label("location: " + component.getLocation() + "");
//            File file = new File ("./images/redShiba.png");
//             URL fileURL = null;
//            try {
//                fileURL = file.toURI().toURL();
//            } catch (MalformedURLException ex) {
//                Logger.getLogger(ComponentEditView.class.getName()).log(Level.SEVERE, null, ex);
//            }
//   Image image = new Image(fileURL.toExternalForm());
// 
//         // simple displays ImageView the image as is
//         ImageView pic = new ImageView();
//            pic.setFitHeight(150);
//    pic.setFitWidth(100);
//         pic.setImage(image);
 
	getChildren().addAll(label2,label3, label, label4, label5 );
      
     
        
      
        }
        //captionVBox.getChildren().add(btSubmit);
      
	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	

      
	// SETUP THE EVENT HANDLERS
	
	
        
    }

   


    
    public ComponentEditView getComponentEditView(){
        return this;
    }
    public Component getComponent(){
        return component;
    }
  public void setComponent(Component component){
      this.component=component;
  }
  public void setSelected(){
      this.getStyleClass().remove("component_edit_view");
      this.getStyleClass().add("component_edit_view_select");
      this.selected=true;
  }
  
  public void setDeSelected(){
      this.selected=false;
  }
  public boolean isSelected(){
      if (this.selected==true){
          return true;
      }
      else{
          return false;
      }
  }
}
