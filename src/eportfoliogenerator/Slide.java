package eportfoliogenerator;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
//import ssm.view.SlideEditView;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class Slide {
    String imageFileName;
    String imagePath;
    String caption;
    SlideEditView slideEdit;
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public Slide(String initImageFileName, String initImagePath,String initCaption) {
        
	imageFileName = initImageFileName;
	imagePath = initImagePath;
        caption=initCaption;
        this.slideEdit=slideEdit;
    }
    public SlideEditView getSlideEdit(){
        return slideEdit;
    }
    public void setSlideEdit(SlideEditView slideEdit){
        this.slideEdit=slideEdit;
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getCaption(){ return caption;}
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    public void setCaption(String initCaption){
        caption = initCaption;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
}
