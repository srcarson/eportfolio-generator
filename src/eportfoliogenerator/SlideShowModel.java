  package eportfoliogenerator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


//import ssm.view.SlideShowMakerView;
//  import ssm.view.SlideEditView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    private ObservableList<Slide> slides = FXCollections.observableArrayList();
    Slide selectedSlide;
    ObservableList<SlideEditView> slideEditors;
    
    public SlideShowModel(SlideShowMakerView initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
        slideEditors = FXCollections.observableArrayList();
	reset();	
       title = title;
	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
	selectedSlide = initSelectedSlide;
        
        
    }
    
    public void setTitle(String title) { 
	this.title=title;
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	getSlides().clear();
	
	title = "";
       
        //TitleField.setText(title);
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath, String initCaption) {
        try{
	Slide slideToAdd = new Slide(initImageFileName, initImagePath, initCaption);
	    getSlides().add(slideToAdd);
	selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
        } catch (Exception e){
     
	String missing = "missing";
	   
	   // System.exit(0);
      
    }
     
    }
    
    
     public void removeSlide( ) {
	getSlides().remove(getSelectedSlide());
	ui.reloadSlideShowPane(this);
    }
      public void moveSlideUp( ) {
	int temp = getSlides().indexOf(getSelectedSlide());
       int temp2 = getSlides().indexOf(getSelectedSlide())-1;
       getSlides().add(temp2,getSelectedSlide());
       getSlides().remove(temp+1);
       System.out.println(temp);
       System.out.println(temp2);
        //Slide temp3 = new Slide(null, null, null);
          //getSlides().remove(slides.indexOf(getSelectedSlide()));
        // getSlides().remove(slides.indexOf(getSelectedSlide())-1);
      // getSlides().add(slides.indexOf(getSelectedSlide())-1,temp);
         // getSlides().add(slides.indexOf(getSelectedSlide()),temp2);
        
	ui.reloadSlideShowPane(this);
    }
      public void moveSlideDown( ) {
	
        //Slide temp3 = new Slide(null, null, null);
       // temp3 =temp2;
       // temp2 = temp;
       // temp = temp3;
       // getSlides().remove(slides.indexOf(getSelectedSlide())+1);
        int temp = getSlides().indexOf(getSelectedSlide());
       int temp2 = getSlides().indexOf(getSelectedSlide())+1;
       getSlides().add(temp2+1,getSelectedSlide());
       getSlides().remove(temp);
       System.out.println(temp);
       System.out.println(temp2);
         //getSlides().remove(slides.indexOf(getSelectedSlide()));
          //getSlides().add(slides.indexOf(getSelectedSlide()),temp2);
          //getSlides().add(slides.indexOf(getSelectedSlide())+1,temp);
        
	ui.reloadSlideShowPane(this);
    }
     
     public void selectSlide(){
         
         ui.reloadSlideShowPane(this);
        
     }

    /**
     * @param slides the slides to set
     */
    public void setSlides(ObservableList<Slide> slides) {
        this.slides = slides;
    }
}