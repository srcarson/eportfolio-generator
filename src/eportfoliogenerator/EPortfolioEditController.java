/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import javafx.scene.control.Tab;

/**
 *
 * @author Scott
 */
public class EPortfolioEditController {
     private EPortfolioGeneratorView ui;
  //PropertiesManager props = PropertiesManager.getPropertiesManager();
	String DEFAULT_CAPTION = "default caption  ";
    /**
     * This constructor keeps the UI for later.
     */
    public EPortfolioEditController(EPortfolioGeneratorView initUI) {
	ui = initUI;
    }
     public void processAddPageRequest() {
	EPortfolioModel EPortfolio = ui.getEPortfolio();
        
        
        Tab tab = new Tab("Set title");
        
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	EPortfolio.addPage(tab);
    }
    public void processRemovePageRequest() {
	EPortfolioModel EPortfolio = ui.getEPortfolio();
	EPortfolio.removePage();
    //EPortfolio.removePage();
    }
}
