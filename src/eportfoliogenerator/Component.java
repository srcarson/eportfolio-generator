package eportfoliogenerator;

import java.util.List;
import javafx.scene.control.ListView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Scott
 */
public class Component {
    private String type;
    private String file="";
    private String caption="";
    private String text="";
    private String height="";
    private String width="";
    String initFile="";
   private List< String> hyperlinkText;
   private List<String> hyperLink;
      private ComponentEditView componentEdit;
    private String location="";
    private String font="";
    String video="";
     SlideShowFileManager ssfm = new SlideShowFileManager();
    SlideShowMakerView ssmv = new SlideShowMakerView(ssfm);
     SlideShowModel slideShow= new SlideShowModel(ssmv);
    List<String> list;
    private List<String> strings;
    private ListView<String> listview= new ListView();
    
    public Component( List<String> strings, String file, String type, String caption, String text, String height, String width, List<String> hyperlinkText, List<String> hyperLink, String location, String font, ListView listview, String video, SlideShowModel slideShow){
        if(type.equals("header")){
            this.type=type;
            this.text=text;
        }
        if(type.equals("list")){
            this.type=type;
            this.listview=listview;
            
        }
         if(type.equals("paragraph")){
            this.type=type;
            this.strings=strings;
            this.text=text;
            this.hyperLink = hyperLink;
            this.hyperlinkText = hyperlinkText;
            this.font=font;
            
        }
           if(type.equals("video")){
            this.type=type;
           this.video=video;
       this.caption=caption;
       this.height=height;
       this.width=width;
            
        }
               if(type.equals("image")){
            this.type=type;
            this.caption=caption;
         this.location=location;
         this.height = height;
         this.width = width;
         this.file=file;
           
            
        }
                    if(type.equals("slideshow")){
            this.type=type;
            
            this.slideShow=slideShow;
            
        }
    }
    public String getVideo(){
        return video;
    }
    
    public void setVideo(String video){
        this.video=video;
    }
    public String getType(){
        return type;
    }
    public String getText(){
        return text;
    }
      public ComponentEditView getComponentEdit(){
        return componentEdit;
    }
      public String getInItFile(){
          return initFile;
      }
         public void setInItFile(){
         this.initFile=initFile;
      }
    public void setComponentEdit(ComponentEditView componentEdit){
        this.componentEdit=componentEdit;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the file
     */
    public String getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     * @return the caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * @param caption the caption to set
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the height
     */
    public String getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * @return the width
     */
    public String getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(String width) {
        this.width = width;
    }

    /**
     * @return the hyperlinkText
     */

    /**
     * @return the Location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param Location the Location to set
     */
    public void setLocation(String Location) {
        this.location = Location;
    }

    /**
     * @return the font
     */
    public String getFont() {
        return font;
    }

    /**
     * @param font the font to set
     */
    public void setFont(String font) {
        this.font = font;
    }

    /**
     * @return the listview
     */
    public ListView<String> getListview() {
        return listview;
    }
    public List<String> getList(){
        return list;
    }

    /**
     * @param listview the listview to set
     */
    public void setListview(ListView<String> listview) {
        this.listview = listview;
    }

    /**
     * @return the slideShow
     */
    public SlideShowModel getSlideShow() {
        return slideShow;
    }

    /**
     * @param slideShow the slideShow to set
     */
    public void setSlideShow(SlideShowModel slideShow) {
        this.slideShow = slideShow;
    }

    /**
     * @return the hyperlinkText
     */
    public List< String> getHyperlinkText() {
        return hyperlinkText;
    }

    /**
     * @param hyperlinkText the hyperlinkText to set
     */
    public void setHyperlinkText(List< String> hyperlinkText) {
        this.hyperlinkText = hyperlinkText;
    }

    /**
     * @return the hyperLink
     */
    public List<String> getHyperLink() {
        return hyperLink;
    }

    /**
     * @param hyperLink the hyperLink to set
     */
    public void setHyperLink(List<String> hyperLink) {
        this.hyperLink = hyperLink;
    }

    /**
     * @return the strings
     */
    public List<String> getStrings() {
        return strings;
    }

    /**
     * @param strings the strings to set
     */
    public void setStrings(List<String> strings) {
        this.strings = strings;
    }

 

   
    
}
