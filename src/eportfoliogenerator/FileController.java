/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 *
 * @author Scott
 */
public class FileController {
        private boolean saved;
    public boolean quit;
private boolean yesResponse;
    // THE APP UI
    private EPortfolioGeneratorView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private EPortfolioFileManager EPortfolioIO;

       public FileController(EPortfolioGeneratorView initUI, EPortfolioFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        EPortfolioIO = initSlideShowIO;
    }
       public void handleSaveEPortfolioRequest(){
           try{
             EPortfolioModel EPortfolio = ui.getEPortfolio();
	    
            // SAVE IT TO A FILE
            EPortfolioIO.saveEPortfolio(EPortfolio);
    
            // MARK IT AS SAVED
            saved = true;
            int i=0;
        String index="";
        File htm = new File("");
        File htmlDest = new File("");
        while(i<EPortfolio.getPages().size()){
              System.out.println(i);
        new File("./Sites/" + EPortfolio.getTitle()).mkdirs();
        
        index ="index";
           if(i>0){
        index+=("" + (i+1) + "");
    }
           new File("./Sites/" + EPortfolio.getTitle()+ "/" + index).mkdirs();
      htm = new File( "./HTML5Application/public_html/index.html");
 
       htmlDest = new File( "./Sites/" + EPortfolio.getTitle()+ "/" + index + "/" + index + ".html");
try{
Files.copy(htm.toPath(), htmlDest.toPath());
} catch(IOException e){
    
}
 File css =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getLayout() + ".css");
      File cssDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getLayout() + ".css");
try{
Files.copy(css.toPath(), cssDest.toPath());
} catch(IOException e){
    
}
 File color =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getColor() + ".css");
      File colorDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getColor() + ".css");
try{
Files.copy(color.toPath(), colorDest.toPath());
} catch(IOException e){
    
}
 File font =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getFont() + ".css");
      File fontDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getFont() + ".css");
try{
Files.copy(font.toPath(), fontDest.toPath());
} catch(IOException e){
    
}
    File js =  new File("./HTML5Application/public_html/ePortfolio.js");

        
           File jsDest = new File( "./Sites/" + EPortfolio.getTitle() + "/ePortfolio.js");
   
     
       
try{
Files.copy(js.toPath(), jsDest.toPath());
} catch(IOException e){
    
}
File json  = new File("./data/eportfolios/" + EPortfolio.getTitle() + ".json");
 File jsonDest = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index + "/test" + ".json");
try{
Files.copy(json.toPath(), jsonDest.toPath());
} catch(IOException e){
    
}

//Delete if tempFile exists
File fileTemp = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index + "/test" + ".json");
if (fileTemp.exists()){
    System.out.println("delete");
    fileTemp.delete();
}
 json  = new File("./data/eportfolios/" + EPortfolio.getTitle() + ".json");
  jsonDest = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index+  "/test" + ".json");
try{
Files.copy(json.toPath(), jsonDest.toPath());
} catch(IOException e){
    
}
       File file = new File(EPortfolio.getPages().get(i).getBannerImage());
       
          BufferedImage image = null;
        try {
          
            
            image = ImageIO.read(file);
            
            ImageIO.write(image, "jpg",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            ImageIO.write(image, "gif",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            ImageIO.write(image, "png",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            
        } catch (IOException e) {
        	
        }
           
        
        i++;}
      
           }catch (IOException ioe){
               
           }
       }
        public void handleSiteViewEPortfolioRequest(){
           try{
               
             EPortfolioModel slideShowToSave = ui.getEPortfolio();
	    String x =slideShowToSave.getTitle();
           // slideShowToSave.setTitle("test");
            // SAVE IT TO A FILE
            EPortfolioIO.saveEPortfolio(slideShowToSave);
slideShowToSave.setTitle(x);
            // MARK IT AS SAVED
            saved = true;
           }catch (IOException ioe){
               
           }
       }
           public void handleSaveAsEPortfolioRequest(){
                EPortfolioModel EPortfolio = ui.getEPortfolio();
           try{
               
               VBox pane =new VBox();
 Scene scene = new Scene(pane); 
  // scene.getStylesheets().add(STYLE_SHEET_UI);
           Stage stage = new Stage();
    //       stage.setTitle(sentence);
           stage.setScene(scene);
           Label Sentence = new Label("File Name");
           TextField text = new TextField();
       pane.getChildren().addAll(Sentence, text);
           Button okButton = new Button("OK");
           pane.getChildren().add(okButton);
           pane.getStyleClass().add("background");
           okButton.setOnMouseClicked(e->{
               EPortfolio.setTitle(text.getText());
               stage.close();
           });
           stage.showAndWait();
           
          
                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                // @todo
            
           //  EPortfolioModel slideShowToSave = ui.getEPortfolio();
	    
            // SAVE IT TO A FILE
            EPortfolioIO.saveEPortfolio(EPortfolio);
              int i=0;
        String index="";
        File htm = new File("");
        File htmlDest = new File("");
        while(i<EPortfolio.getPages().size()){
              System.out.println(i);
        new File("./Sites/" + EPortfolio.getTitle()).mkdirs();
        
        index ="index";
           if(i>0){
        index+=("" + (i+1) + "");
    }
           new File("./Sites/" + EPortfolio.getTitle()+ "/" + index).mkdirs();
      htm = new File( "./HTML5Application/public_html/index.html");
 
       htmlDest = new File( "./Sites/" + EPortfolio.getTitle()+ "/" + index + "/" + index + ".html");
try{
Files.copy(htm.toPath(), htmlDest.toPath());
} catch(IOException e){
    
}
 File css =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getLayout() + ".css");
      File cssDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getLayout() + ".css");
try{
Files.copy(css.toPath(), cssDest.toPath());
} catch(IOException e){
    
}
 File color =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getColor() + ".css");
      File colorDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getColor() + ".css");
try{
Files.copy(color.toPath(), colorDest.toPath());
} catch(IOException e){
    
}
 File font =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getFont() + ".css");
      File fontDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getFont() + ".css");
try{
Files.copy(font.toPath(), fontDest.toPath());
} catch(IOException e){
    
}
    File js =  new File("./HTML5Application/public_html/ePortfolio.js");

        
           File jsDest = new File( "./Sites/" + EPortfolio.getTitle() + "/ePortfolio.js");
   
     
       
try{
Files.copy(js.toPath(), jsDest.toPath());
} catch(IOException e){
    
}
File json  = new File("./data/eportfolios/" + EPortfolio.getTitle() + ".json");
 File jsonDest = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index + "/test" + ".json");
try{
Files.copy(json.toPath(), jsonDest.toPath());
} catch(IOException e){
    
}

//Delete if tempFile exists
File fileTemp = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index + "/test" + ".json");
if (fileTemp.exists()){
    System.out.println("delete");
    fileTemp.delete();
}
 json  = new File("./data/eportfolios/" + EPortfolio.getTitle() + ".json");
  jsonDest = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index+  "/test" + ".json");
try{
Files.copy(json.toPath(), jsonDest.toPath());
} catch(IOException e){
    
}
       File file = new File(EPortfolio.getPages().get(i).getBannerImage());
       
          BufferedImage image = null;
        try {
          
            
            image = ImageIO.read(file);
            
            ImageIO.write(image, "jpg",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            ImageIO.write(image, "gif",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            ImageIO.write(image, "png",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            
        } catch (IOException e) {
        	
        }
           
        
        i++;}

            // MARK IT AS SAVED
            saved = true;
           }catch (IOException ioe){
               
           }
       }
       public void handleLoadEPortfolioRequest(){
            
           boolean continueToOpen = true;
           if (!saved) {
               // THE USER CAN OPT OUT HERE WITH A CANCEL
               //continueToOpen = promptToSave();
           }
           promptToOpen();
       }
         private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
      //  slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		EPortfolioModel EPortfolioToLoad = ui.getEPortfolio();
                EPortfolioIO.loadEPortfolio(EPortfolioToLoad, selectedFile.getAbsolutePath());
                ui.reloadEPortfolioPane(EPortfolioToLoad);
                saved = true;
              // ui.updateToolbarControls(saved);
            } catch (Exception e) {

                // @todo
            }
        }
    }

       public void handleNewEPortfolioRequest() {
        //try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
//            if (!saved) {
//                // THE USER CAN OPT OUT HERE WITH A CANCEL
//                continueToMakeNew = promptToSave();
//            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
//            if (continueToMakeNew) {
//                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
              EPortfolioModel EPortfolio = ui.getEPortfolio();
                EPortfolio.reset();
                EPortfolio.setTitle("default");
//		slideShow.reset();
//               slideShow.setTitle(slideShow.getTitle());
//                saved = false;
//
//                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
//                // THE APPROPRIATE CONTROLS
              ui.updateToolbarControls(saved);
              ui.reloadEPortfolioPane(EPortfolio);
//ui.reloadSlideShowPane(slideShow);
//PropertiesManager props = PropertiesManager.getPropertiesManager();
	//String sentence = props.getProperty(LanguagePropertyType.NEW_SLIDE_SHOW);
VBox pane =new VBox();
 Scene scene = new Scene(pane); 
  // scene.getStylesheets().add(STYLE_SHEET_UI);
           Stage stage = new Stage();
    //       stage.setTitle(sentence);
           stage.setScene(scene);
           Label Sentence = new Label("A new Eportfolio has been created");
       pane.getChildren().add(Sentence);
           Button okButton = new Button("OK");
           pane.getChildren().add(okButton);
           pane.getStyleClass().add("background");
           okButton.setOnMouseClicked(e->{
               stage.close();
           });
           stage.showAndWait();
           
                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                // @todo
            }
        //} catch (IOException ioe) {
   
        // ErrorHandler errorHandler = ui.getErrorHandler();
    //PropertiesManager props = PropertiesManager.getPropertiesManager();
	//String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
	  //  errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
     

}
