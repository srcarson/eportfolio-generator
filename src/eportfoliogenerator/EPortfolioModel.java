/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author Scott
 */
public class EPortfolioModel {
   EPortfolioGeneratorView ui;
    String title="";
    ObservableList<Page> pages;
    Page selectedPage;
    private String name = "";
    Tab selectedTab;
    ObservableList<PageEditView> slideEditors;
    
     public EPortfolioModel(EPortfolioGeneratorView initUI) {
	ui = initUI;
	pages = FXCollections.observableArrayList();
        slideEditors = FXCollections.observableArrayList();
	//reset();	
       // PropertiesManager props = PropertiesManager.getPropertiesManager();
	//title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
    }
     public void addPage(Tab tab){
         System.out.println(" name: " + tab.getText());
         
         Page pageToAdd = new Page(tab.getText(), ui);
         pages.add(pageToAdd);
         selectedPage=pageToAdd;
         selectedTab = tab;
        
         tab.setContent(pageToAdd);
         ui.addPageTab(tab);
       //  ui.reloadEPortfolioPane(this);
        
     }
     public void addPage(String pagename,String footer,String font ,String color,String bannerImage,JsonArray components,String layout){
         Page page = new Page(pagename, ui);
         page.setFooter(footer);
         System.out.println(footer);
         font = font.substring(0, font.length()-4);
         page.setFont(font);
         color = color.substring(0, color.length()-4);
         page.setColor(color);
         System.out.println(page.getFont());
         page.setBannerImage(bannerImage);
         layout = layout.substring(0, layout.length()-4);
         page.setLayout(layout);
         System.out.println(" color " + color);
for (int i = 0; i < components.size(); i++) {
	    JsonObject PageJso = components.getJsonObject(i);
	    page.addComp(	PageJso.getJsonArray("list"),
					PageJso.getString("file"),
                                        PageJso.getString("type"),
                                        PageJso.getString("caption"),
                                         PageJso.getString("text"),
                                          PageJso.getString("height"),
                                          //PageJso.getJsonArray("components"),
                                        PageJso.getString("width"),
                                            PageJso.getJsonArray("list"),
                                             PageJso.getJsonArray("list"),
                                             PageJso.getString("location"),
                                          PageJso.getString("font"),
                                          PageJso.getJsonArray("list"),
                                          PageJso.getString("video"),
                                          PageJso.getJsonArray("slideShow"),
                                          PageJso.getString("slideShowTitle"));
                                          
	}
Tab tab = new Tab(pagename);
System.out.println("wtf: " + pagename);
page.getComponents();
tab.setContent(page);
addPage(tab);
        }
     
     public void setTitle(String title){
        this.title=title;
     }
     public void reset(){
         pages.clear();
	
	title = ("default");
       
        //TitleField.setText(title);
	selectedPage = null;
        selectedTab = null;
     }
     public String getTitle(){
         return title;
     }
     public void removePage(){
         
         
         ui.removePageTab(selectedTab);
     }
      public ObservableList<Page> getPages() {
	return pages;
      }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
