package eportfoliogenerator;

import java.io.File;
import java.net.URL;
import javafx.geometry.Rectangle2D;
import javafx.scene.text.Font;
import javafx.scene.Group;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javafx.scene.Scene;
import java.io.FileWriter;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.geometry.Pos;
import javafx.scene.layout.Priority;
import javafx.application.Application;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import java.nio.file.StandardOpenOption;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    boolean ok;
    Scene primaryScene;
    ImageView imageSelectionView = new ImageView();
    int slideNumber=0;
    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;
    int totalSlides=0;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button exitButton;
    Button titleButton;
    Button OKButton;
    Button CancelButton;
    TextField TitleField;
    int count=0;
    int selected=-1;
    
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveSlideUpButton;
    Button moveSlideDownButton;
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
 

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
  SlideFileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);
        

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }
public void setSlideShow(SlideShowModel slideShow){
    this.slideShow=slideShow;
}
    public Stage getWindow() {
        
	return primaryStage;
    }

   

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
     
        //primaryStage.setTitle(slideShow.getTitle());
	initWindow(windowTitle);
        
        ok=false;
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
        //slideEditToolbar.setSpacing(100);
       // slideEditToolbar.setAlignment(Pos.CENTER);
	slideEditToolbar.getStyleClass().add("slide_show_edit_vbox");
	addSlideButton = this.initChildButton(slideEditToolbar,		"Add.png",	    "Add Slide",	   "vertical_toolbar_button",  false);
        removeSlideButton = this.initChildButton(slideEditToolbar,		"Remove.png",	    "Remove Slide",	    "vertical_toolbar_button",  true);
        moveSlideUpButton = this.initChildButton(slideEditToolbar,		"MoveUp.png",	    "Move Up",	    "vertical_toolbar_button",  true);
	 moveSlideDownButton = this.initChildButton(slideEditToolbar,		"MoveDown.png",	   "Move Down",	    "vertical_toolbar_button",  true);
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
   
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
        
    }
public void setTrue(){
    ok=true;
}
public boolean getOk(){
    return ok;
}
    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new SlideFileController(this, fileManager);
	
	OKButton.setOnMouseClicked(e->{
            
        });
        titleButton.setOnMouseClicked(e->{
            if(TitleField.getText().equals("")){
                
                String title = ("default title");
                TitleField.setText(title);
                String windowTitle = "slideShowMaker";
             primaryStage.setTitle(windowTitle + "-" + TitleField.getText());
             
             slideShow.setTitle("default");
      //  fileController.markAsEdited();
            }
            else{
             
            slideShow.setTitle(TitleField.getText());
               String windowTitle = "SlideShowMaker";
             primaryStage.setTitle(windowTitle + "-" + TitleField.getText());
       // fileController.markAsEdited();
            }
    });
          //  fileController.handleTextEditRequest();
        //});
        
      
	
	
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
            
           // fileController.markAsEdited();
	    editController.processAddSlideRequest();
            viewSlideShowButton.setDisable(false);
              slideShow.setSelectedSlide(null);
            removeSlideButton.setDisable(true);
             moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
	});
        removeSlideButton.setOnAction(e->{
           
          //  fileController.markAsEdited();
            editController.processRemoveSlideRequest();
             if(slideShow.getSlides().size()==0){
            viewSlideShowButton.setDisable(true);
        }
              slideShow.setSelectedSlide(null);
            removeSlideButton.setDisable(true);
            moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
        });
            moveSlideUpButton.setOnAction(e->{
            //  fileController.markAsEdited();
                editController.processmoveSlideUpRequest();
             slideShow.setSelectedSlide(null);
            removeSlideButton.setDisable(true);
            moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
        });
               moveSlideDownButton.setOnAction(e->{
               //    fileController.markAsEdited();
                editController.processmoveSlideDownRequest();
           slideShow.setSelectedSlide(null);
            removeSlideButton.setDisable(true);
            moveSlideUpButton.setDisable(true);
             moveSlideDownButton.setDisable(true);
        });
               OKButton.setOnAction(e->{
                   primaryStage.close();
                   setTrue();
               });
               CancelButton.setOnAction(e->{
                   primaryStage.close();
               });
               
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
  
    public void getPrevImage(int slide){
        
         String imagePath = slideShow.getSlides().get(slide-1).getImagePath() + "/" + slideShow.getSlides().get(slide-1).getImageFileName();
	File file = new File(imagePath);
        Image slideImage;
	try {
	     //GET AND SET THE IMAGE
	   URL fileURL = file.toURI().toURL();
	    slideImage = new Image(fileURL.toExternalForm());
	  
           imageSelectionView.setImage(slideImage);
	     
	    // AND RESIZE IT
	  double scaledHeight = 500;
	    double perc = scaledHeight / slideImage.getHeight();
	    double scaledWidth = slideImage.getWidth() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
            
	    
	   // System.exit(0);
	    // @todo - use Error handler to respond to missing image
	}
       
        
    }
      public void getNextImage(int slide){
        
         String imagePath = slideShow.getSlides().get(slide+1).getImagePath() + "/" + slideShow.getSlides().get(slide+1).getImageFileName();
	File file = new File(imagePath);
        Image slideImage;
	try {
	     //GET AND SET THE IMAGE
	   URL fileURL = file.toURI().toURL();
	    slideImage = new Image(fileURL.toExternalForm());
	  
           imageSelectionView.setImage(slideImage);
	     
	    // AND RESIZE IT
	  double scaledHeight = 500;
	    double perc = scaledHeight / slideImage.getHeight();
	    double scaledWidth = slideImage.getWidth() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
               
	}
      }
    public void updateStage(){
        
    }
    public void handlePrevButton(){
             slideNumber-=1;
         }
    public void handleNextButton(){
             slideNumber+=1;
         }
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        fileToolbarPane.getStyleClass().add("horizontal");
       
        //fileToolbarPane.setHgap(10);
        //  fileToolbarPane.setVgap(10);
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	
	
        TitleField = new TextField(slideShow.getTitle());
        
        
        TitleField.getStyleClass().add("horizontal_toolbar_button");
	String title = "Enter Title";
        titleButton = new Button(title);
        titleButton.getStyleClass().add("font");
       // titleButton.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
        fileToolbarPane.getChildren().add(TitleField);
        fileToolbarPane.getChildren().add(titleButton);
         OKButton = initChildButton(fileToolbarPane, "ok.png" ,	"Finish SlideShow","horizontal_toolbar_button", false);
        CancelButton = initChildButton(fileToolbarPane, "DeleteOrig.png" ,	"Finish SlideShow","horizontal_toolbar_button", false);
        OKButton.getStyleClass().add("horizontal_toolbar_button");
        CancelButton.getStyleClass().add("horizontal_toolbar_button");
      //VBox VBoxTitle=new VBox();
    // VBoxTitle.addCaptionLabel()
       
    }
    public void handleSlideSelect(SlideEditView slide){
        
        slide.setBorder(null);
        slide.updateSlideImage();
    }
    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
      
            primaryStage.setTitle(windowTitle);
        
	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
        ssmPane.getStyleClass().add("background");
	ssmPane.setTop(fileToolbarPane);	
        ssmPane.setLeft(workspace);
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add("/eportfoliogenerator/SlideShowMakerStyle.css");

	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	
	String imagePath = "file:" + "./images/icons/" + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
        if(slideShow.getSlides().size()==0){
	viewSlideShowButton.setDisable(true);
        }
        else{
          viewSlideShowButton.setDisable(false);  
        }
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
        TitleField.setText(slideShow.getTitle());
        
	String stageTitle = "title";
        primaryStage.setTitle(stageTitle + "-" + slideShow.getTitle());
    }
    

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
        slideShow.setTitle(slideShowToLoad.getTitle());
        int slideNum=-1;
	slidesEditorPane.getChildren().clear();
        
        // slideShow.setSelectedSlide(null);
        
	for (Slide slide : slideShowToLoad.getSlides()) {
       
      
	    SlideEditView slideEditor = new SlideEditView(slide);
         slide.setSlideEdit(slideEditor);
            
            //Background background = slideEditor.getBackground();
            //reloadSlideShowPane(slideShowToLoad);
            slideEditor.captionTextField.setOnKeyPressed(e->{
              //  fileController.markAsEdited();
            });
             ImageSelectionController imageController;
            
            imageController = new ImageSelectionController();
            slideEditor.imageSelectionView.setOnMousePressed(e->{
             //   fileController.markAsEdited();
                
	
            
	    imageController.processSelectImage(slide, slideEditor);
             slide.getSlideEdit().getStyleClass().remove("slide_edit_view_select"); 
             slide.getSlideEdit().getStyleClass().add("slide_edit_view"); 
               moveSlideDownButton.setDisable(true);
                moveSlideUpButton.setDisable(true);
                removeSlideButton.setDisable(true);
            });
            slideEditor.setOnMouseClicked(e->{
         
                
                moveSlideDownButton.setDisable(true);
                moveSlideUpButton.setDisable(true);
              //handleSelect(slideShowToLoad, slide);  
               
            
              
              
           
              //editController.processSelectRequest();
           // slideEditor.updateSlideImage();
            
             slideShow.setSelectedSlide(slide);
              handleSlideSelected();
                removeSlideButton.setDisable(false);
                if(!(slide.equals(slideShowToLoad.getSlides().get(0)))){
                    moveSlideUpButton.setDisable(false);
                }
                if(!(slide.equals(slideShowToLoad.getSlides().get(slideShowToLoad.getSlides().size()-1)))){
                    moveSlideDownButton.setDisable(false);
                }
                 
            });
          
         
               
	    slidesEditorPane.getChildren().add(slideEditor);
              
           // slideEditor.setBackground(background);
            
	}
    }
    public void handleSlideSelected(){
        Slide slide= slideShow.getSelectedSlide();
         slide.getSlideEdit().getStyleClass().remove("slide_edit_view_select"); 
     slide.getSlideEdit().getStyleClass().add("slide_edit_view_select"); 
                
                 for(int i=0; i<slideShow.getSlides().size(); i++){
                     if(!(slide.equals(slideShow.getSlides().get(i)))){
                  
                   slideShow.getSlides().get(i).getSlideEdit().getStyleClass().remove("slide_edit_view_select");
              slideShow.getSlides().get(i).getSlideEdit().getStyleClass().add("slide_edit_view"); 
               }
                }
   }

}
