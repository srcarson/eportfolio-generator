/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package eportfoliogenerator;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
/**
 *
 * @author Scott
 */
public class EPortfolioFileManager {
     public static String Pagename = "Pagename";
     public static String currentPage = "currentPage";
      public static String JSON_TITLE = "title";
        public static String JSON_Pages = "pages";
    String banner="no";
     EPortfolioGeneratorView ui;
     
      public void saveEPortfolio(EPortfolioModel EPortfolio) throws IOException {
        // BUILD THE FILE PATH
        String ePortTitle = "" + EPortfolio.getTitle();
        
        String jsonFilePath = "./data/eportfolios/" + ePortTitle + ".json";
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);  
           
        // BUILD THE SLIDES ARRAY
      
    
        JsonArray pagesJsonArray = makePagesJsonArray(EPortfolio.getPages());
        
        // NOW BUILD1 THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject courseJsonObject = Json.createObjectBuilder()
                                    .add(JSON_TITLE, EPortfolio.getTitle())
                .add("name", EPortfolio.getName()+ "'s ePortfolio")
                                    .add(JSON_Pages, pagesJsonArray)
                .build();
        
        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(courseJsonObject);
    }
      public void loadEPortfolio(EPortfolioModel EPortfolioToLoad, String jsonFilePath) {
        // LOAD THE JSON FILE WITH ALL THE DATA
        try{
        JsonObject json = loadJSONFile(jsonFilePath);
        
        // NOW LOAD THE COURSE
	EPortfolioToLoad.reset();
        EPortfolioToLoad.setTitle(json.getString(JSON_TITLE));
        EPortfolioToLoad.setName(json.getString("name"));
        
        JsonArray jsonPagesArray = json.getJsonArray("pages");
        JsonArray jsonComponentsArray = json.getJsonArray("components");
        for (int i = 0; i < jsonPagesArray.size(); i++) {
	    JsonObject PageJso = jsonPagesArray.getJsonObject(i);
	    EPortfolioToLoad.addPage(	PageJso.getString("Pagename"),
					PageJso.getString("footer"),
                                        PageJso.getString("font"),
                                        PageJso.getString("color"),
                                         PageJso.getString("BannerImage"),
                                          PageJso.getJsonArray("components"),
                                        PageJso.getString("layout"));
	}
        }catch (Exception e){

}
    } 
      private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        try{
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
        }catch (Exception e){
   
    }
        return null;
    }
    
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName)  {
        
    try{
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
         
        return items;
        }catch (Exception e){

      
    }
     return null;
    }
      private JsonArray makeComponentsJsonArray(List<Component> components){
          
       try{
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Component component : components) {
	    JsonObject jso = makeComponentJsonObject(component);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
        } 
   catch (Exception e){
       System.out.print("wwww");
// ErrorHandler errorHandler = ui.getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
      
    }
     return null;
    }
     private JsonArray makePagesJsonArray(List<Page> pages) {
        try{
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Page page : pages) {
	    JsonObject jso = makePageJsonObject(page);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
        } 
   catch (Exception e){
       System.out.print("wwww");
// ErrorHandler errorHandler = ui.getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
      
    }
     return null;
    }
     private JsonArray makeListJsonArray(List<String> texts) {
        try{
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String text : texts) {
	    JsonObject jso = makeTextJsonObject(text);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
        } 
   catch (Exception e){
       System.out.print("wwww");
// ErrorHandler errorHandler = ui.getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
      
    }
     return null;
    }
        private JsonArray makeSlideShowJsonArray(List<Slide> slides) {
        try{
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Slide slide : slides) {
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
        } 
   catch (Exception e){
       System.out.print("wwww");
// ErrorHandler errorHandler = ui.getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
      
    }
     return null;
    }
     private JsonObject makeComponentJsonObject(Component component){
        try{
        JsonArray listJsonArray = makeListJsonArray(component.getListview().getItems());      
           JsonArray slideShowJsonArray = makeSlideShowJsonArray(component.getSlideShow().getSlides());  
        JsonObject jso = Json.createObjectBuilder()
                .add("type", component.getType())
		.add("text", component.getText())
		.add("font", component.getFont())
                .add("file", component.getFile() + ".jpg")
                .add("location", component.getLocation())
                 .add("height", component.getHeight())
                  .add("width", component.getWidth()) 
                .add("caption", component.getCaption())
                .add("video", component.getVideo() +".mp4")
               .add("list", listJsonArray)
                  .add("slideShowTitle", component.getSlideShow().getTitle())
                .add("slideShow", slideShowJsonArray)
        .build();
	return jso;
    }
        catch (Exception e){
            System.out.println("xx");
//       ErrorHandler errorHandler = ui.getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
      
    }
     return null;
    }
    
    private JsonObject makeTextJsonObject(String text) {
      
        try{
          
        JsonObject jso = Json.createObjectBuilder()
		.add("text", text)
		
                
                        
		.build();
	return jso;
    }
        catch (Exception e){
            System.out.println("xx");
//       ErrorHandler errorHandler = ui.getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
      
    }
     return null;
    }
       private JsonObject makeSlideJsonObject(Slide slide) {
      
        try{
          
        JsonObject jso = Json.createObjectBuilder()
		.add("caption", slide.getCaption())
                .add("file", slide.getImageFileName() + ".jpg")
		
                
                        
		.build();
	return jso;
    }
        catch (Exception e){
            System.out.println("xx");
//       ErrorHandler errorHandler = ui.getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
      
    }
     return null;
    }
    
    private JsonObject makePageJsonObject(Page page) {
        JsonArray componentsJsonArray = makeComponentsJsonArray(page.getComponents());
              banner= "BannerImage.jpg";
  
              if(page.getBannerImage()==null){
                  banner="x";
              }
        try{
          
        JsonObject jso = Json.createObjectBuilder()
		.add(Pagename, page.getTitle())
		.add("footer", page.getFooter())
                .add("studentName", page.getStudentName() + "'s ePortfolio")
                .add("layout", page.getLayout() + ".css")
                .add("font", page.getFont()+ ".css")
                .add("color", page.getColor()+".css")
                .add("BannerImage", banner)
                .add("components", componentsJsonArray)
                
                        
		.build();
	return jso;
    }
        catch (Exception e){
            System.out.println("xx");
//       ErrorHandler errorHandler = ui.getErrorHandler();
//    PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String missing = props.getProperty(LanguagePropertyType.FILE_MISSING);
//	    errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, missing , "");
      
    }
     return null;
    }
}
