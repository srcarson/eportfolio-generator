package eportfoliogenerator;


import java.io.File;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.net.URL;
import javafx.geometry.Rectangle2D;
import javafx.scene.text.Font;
import javafx.scene.Group;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javafx.scene.Scene;
import java.io.FileWriter;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.geometry.Pos;
import javafx.scene.layout.Priority;
import javafx.application.Application;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;



 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Scott
 */
public class ComponentProcess {
     ListView<String> listView = new ListView();
    SlideShowFileManager ssfm = new SlideShowFileManager();
    SlideShowMakerView ssmv = new SlideShowMakerView(ssfm);
     SlideShowModel slideShow= new SlideShowModel(ssmv);
     Page page;
     String x="";
     String fileName;
     String locationx="";
        List<String> hype= new ArrayList<String>();
         List<String> strings= new ArrayList<String>();
      List<String> links= new ArrayList<String>();
      String total ="";
     EPortfolioGeneratorView ui;
    public ComponentProcess(String type, EPortfolioGeneratorView ui){
       if(type.equals("image")) {
           this.ui=ui;
              Stage stage = new Stage();
            
                   VBox pane = new VBox();
                   Label locationLabel = new Label("location:");
                   ChoiceBox location = new ChoiceBox();
                   location.getItems().addAll("left", "right", "middle");
                   HBox loc = new HBox();
                  location.getSelectionModel().selectedItemProperty()
            .addListener(e -> {
           locationx= (String)location.getSelectionModel().getSelectedItem(); 
            });
                   loc.getChildren().addAll(locationLabel, location);
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   Label height = new Label("height in px: ");
                   TextField heightField = new TextField();
                   HBox heightBox = new HBox();
                   heightBox.getChildren().addAll(height, heightField);
                   Label width = new Label("width in px:  ");
                   TextField widthField = new TextField();
                   HBox widthBox = new HBox();
                   widthBox.getChildren().addAll(width, widthField);
                   Label caption = new Label("caption:        ");
                   TextField captionField = new TextField();
                   HBox captionBox = new HBox();
                   captionBox.getChildren().addAll(caption, captionField);
                   
                   HBox Hbox = new HBox();
                   Button file = new Button("Choose File");
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll(file, Ok, canc);
                   pane.setSpacing(5);
                   pane.getChildren().addAll(loc, heightBox, widthBox, captionBox, Hbox);
                   
                   file.setOnAction(e->{
                       FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
     
              
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
        File filez = imageFileChooser.showOpenDialog(null);
          String path = filez.getPath().substring(0, filez.getPath().indexOf(filez.getName()));
	     fileName = filez.getName();
          //  component.setInItFile(path+fileName);
	File filem = new File(path+fileName);
       
          BufferedImage image = null;
          Page pagem = ui.getPage();
          int x= ui.getEPortfolio().getPages().indexOf(pagem);
          System.out.println("page number" + x);
          String num="";
          if(x==0){
              
          }
          else{
         num = x +1 + "";
          }
           new File("./Sites/" + ui.getTitle()+ "/index" + num).mkdirs();
        try {
          
            
            image = ImageIO.read(filem);
            
            ImageIO.write(image, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/" + fileName + ".jpg"));
            ImageIO.write(image, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/" + fileName + ".jpg"));
            ImageIO.write(image, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/" + fileName + ".jpg"));
            
        } catch (IOException k) {
        	k.printStackTrace();
        }
           
        
      
                   });
                   
             Ok.setOnAction(e->{
                  Page pagem = ui.getPage();
                 
                 pagem.addComponent(null, fileName, "image", captionField.getText(), null, heightField.getText(), widthField.getText(), null, null, locationx, null, null,null, null);
                stage.close(); 
                stage.close(); 
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   

       }
           if(type.equals("video")) {
              Stage stage = new Stage();
                   VBox pane = new VBox();
                   
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   Label height = new Label("height in px: ");
                   TextField heightField = new TextField();
                   HBox heightBox = new HBox();
                   heightBox.getChildren().addAll(height, heightField);
                   Label width = new Label("width in px:  ");
                   TextField widthField = new TextField();
                   HBox widthBox = new HBox();
                   widthBox.getChildren().addAll(width, widthField);
                 Label caption = new Label("caption:        ");
                   TextField captionField = new TextField();
                   HBox captionBox = new HBox();
                   captionBox.getChildren().addAll(caption, captionField);
                   
                   HBox Hbox = new HBox();
                   Button file = new Button("Choose File");
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll(file, Ok, canc);
                   Hbox.setSpacing(5);
                   pane.getChildren().addAll(heightBox, widthBox, captionBox, Hbox);
                          file.setOnAction(e->{
                       FileChooser imageFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
     
              
	FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	
        imageFileChooser.getExtensionFilters().addAll(mp4Filter);
        File filez = imageFileChooser.showOpenDialog(null);
          String path = filez.getPath().substring(0, filez.getPath().indexOf(filez.getName()));
	     fileName = filez.getName();
          //  component.setInItFile(path+fileName);
	File filem = new File(path+fileName);
       
          BufferedImage image = null;
          Page pagem = ui.getPage();
          int x= ui.getEPortfolio().getPages().indexOf(pagem);
          System.out.println("page number" + x);
          String num="";
          if(x==0){
              
          }
          else{
         num = x +1 + "";
          }
           new File("./Sites/" + ui.getEPortfolio().getTitle()+ "/index" + num).mkdirs();
        File vidDest = new File( "./Sites/" + ui.getEPortfolio().getTitle() + "/index" + num + "/" + fileName + ".mp4");
   
     
       
try{
Files.copy(filem.toPath(), vidDest.toPath());
} catch(IOException k){
    
}
                          });
             Ok.setOnAction(e->{
                   Page pagem = ui.getPage();
                
                 pagem.addComponent(null, null, "video", captionField.getText(), null, heightField.getText(), widthField.getText(), null, null, null, null, null,fileName, null);
                stage.close(); 
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   

       }
           if(type.equals("header")) {
              Stage stage = new Stage();
                   VBox pane = new VBox();
                   
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   Label height = new Label("height in px:");
                   TextField heightField = new TextField();
                   HBox heightBox = new HBox();
                   heightBox.getChildren().addAll(height, heightField);
                   Label width = new Label("width in px:");
                   TextField widthField = new TextField();
                   HBox widthBox = new HBox();
                   widthBox.getChildren().addAll(width, widthField);
                   Label caption = new Label("Header: ");
                   TextField captionField = new TextField();
                   HBox captionBox = new HBox();
                   captionBox.getChildren().addAll(caption, captionField);
                   
                   HBox Hbox = new HBox();
                   Button file = new Button("Choose File");
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll( Ok, canc);
                   pane.getChildren().addAll( captionBox, Hbox);
                   
             Ok.setOnAction(e->{
                Page pagem = ui.getPage();
                String x = captionField.getText();
                 pagem.addComponent(null, null, "header", null, x, null, null, null, null, null, null, null,null, null);
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   

       } if(type.equals("slideshow")) {
           
            SlideShowFileManager fileManager = new SlideShowFileManager();
 SlideShowMakerView view = new SlideShowMakerView(fileManager);

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
            VBox pane = new VBox();
                   Stage stage =new Stage();
                   
                  view.startUI(stage, "SlideShowMaker");
                slideShow=   view.getSlideShow();
              
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   HBox Hbox = new HBox();
                   Hbox.getChildren().addAll( Ok, canc);
                   
   


                  
                   
           view.OKButton.setOnAction(e->{
                Page pagem = ui.getPage();
                int i=0; slideShow.getSlides().size();
               while(i<slideShow.getSlides().size()){
                    fileName = slideShow.getSlides().get(i).getImageFileName();
                File filem = new File(slideShow.getSlides().get(i).getImagePath()+fileName);
       
          BufferedImage image = null;
          
          int x= ui.getEPortfolio().getPages().indexOf(pagem);
          System.out.println("page number" + x);
          String num="";
          if(x==0){
              
          }
          else{
         num = x +1 + "";
          }
           new File("./Sites/" + ui.getTitle()+ "/index" + num).mkdirs();
        try {
          
            
            image = ImageIO.read(filem);
            
            ImageIO.write(image, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/" + fileName + ".jpg"));
            ImageIO.write(image, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/" + fileName + ".jpg"));
            ImageIO.write(image, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/" + fileName + ".jpg"));
            
        } catch (IOException k) {
        	k.printStackTrace();
        }
           
             i++;   }
               try {
                     int x= ui.getEPortfolio().getPages().indexOf(pagem);
          System.out.println("page number" + x);
          String num="";
          if(x==0){
              
          }
          else{
         num = x +1 + "";
          }
          
            File fileA = new File("images/icons/Next.png");
            BufferedImage imageA = null;
            imageA = ImageIO.read(fileA);
            
            ImageIO.write(imageA, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/next.jpg"));
            ImageIO.write(imageA, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/next.jpg"));
            ImageIO.write(imageA, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/next.jpg"));
            
                File fileB = new File("images/icons/prev.png");
            BufferedImage imageB = null;
            imageB = ImageIO.read(fileB);
            
            ImageIO.write(imageB, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/prev.jpg"));
            ImageIO.write(imageB, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/prev.jpg"));
            ImageIO.write(imageB, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/prev.jpg"));
            
                File fileC = new File("images/icons/pause.png");
            BufferedImage imageC = null;
            imageC = ImageIO.read(fileC);
            
            ImageIO.write(imageC, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/pause.jpg"));
            ImageIO.write(imageC, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/pause.jpg"));
            ImageIO.write(imageC, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/pause.jpg"));
            
                   File fileD = new File("images/icons/play.png");
            BufferedImage imageD = null;
            imageD = ImageIO.read(fileD);
            
            ImageIO.write(imageD, "jpg",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/play.jpg"));
            ImageIO.write(imageD, "gif",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num +"/play.jpg"));
            ImageIO.write(imageD, "png",new File("./Sites/" + ui.getEPortfolio().getTitle()+"/" + "index" + num + "/play.jpg"));
        } catch (IOException k) {
        	k.printStackTrace();
        }
      
                   
                   System.out.println(slideShow.getTitle());
                 pagem.addComponent(null, null, "slideshow", null, null, null, null, null, null, null, null, null,null, slideShow);
                stage.close(); 
             });     
           view.CancelButton.setOnAction(e->{  
                stage.close(); 
                
           });
    
                   
       }
           
       if(type.equals("list")) {
              Stage stage = new Stage();
                   VBox pane = new VBox();
                   
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   listView = new ListView<>();
             
              
                   HBox Hbox = new HBox();
           
                   Button Ok = new Button("Ok");
                   Button remove = new Button("Remove");
                   Button Add = new Button("Add");
                 
                   TextField textField = new TextField();
                     HBox more = new HBox();
                   more.getChildren().addAll(Add, textField);
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll( remove, Ok, canc);
                   pane.getChildren().addAll( listView, more, Hbox);
                   remove.setOnAction(e->{
                       String select = listView.getSelectionModel().getSelectedItem();
                       
                       for(int i=0; i< listView.getItems().size(); i++ ){
                           if(select.equals(listView.getItems().get(i))){
                           listView.getItems().remove(i);
                       }
                       }
                   });
                   
                   Add.setOnAction(e->{
                       if(listView.getSelectionModel().getSelectedItem()==null){
                      listView.getItems().addAll(textField.getText());
                      textField.clear();
                       }
                       else{
                          
                           String select = listView.getSelectionModel().getSelectedItem();
                       
                       for(int i=0; i< listView.getItems().size(); i++ ){
                           if(select.equals(listView.getItems().get(i))){
                               
                          listView.getItems().add(i+1, textField.getText());
                               textField.clear();
                               break;
                       }
                       }
                       }
                      
                      
                   });
             Ok.setOnAction(e->{
                 Page pagem = ui.getPage();
                  pagem.addComponent(null, null, "list", null, null, null, null, null, null, null, null, listView,null, null);
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   

       }
           if(type.equals("paragraph")) {
            Stage stage = new Stage();
                   VBox pane = new VBox();
                   ArrayList list = new ArrayList();
                   Scene scene = new Scene(pane);
                   stage.setScene(scene);
                   Label height = new Label("height in px:");
                   TextField heightField = new TextField("100");
                   HBox heightBox = new HBox();
                   heightBox.getChildren().addAll(height, heightField);
                   Label width = new Label("width in px:");
                   TextField widthField = new TextField("50");
                   HBox widthBox = new HBox();
                   widthBox.getChildren().addAll(width, widthField);
                   Label caption = new Label("paragraph:  ");
                   TextArea captionField = new TextArea();
                   
                   HBox captionBox = new HBox();
                   captionBox.getChildren().addAll(caption, captionField);
                        ChoiceBox font = new ChoiceBox();
 font.getItems().addAll("Open Sans", "Montserrat", "Lora", "Ubuntu", "Oxygen");
// font.getSelectionModel().setSelectedItem();
 Label fontLabel = new Label("font:  ");
 HBox fontBox = new HBox();
 fontBox.getChildren().addAll(fontLabel, font);
                   HBox Hbox = new HBox();
                   int i=0;
                   Button file = new Button("Choose File");
                   Button Ok = new Button("Ok");
                   Button canc = new Button("cancel");
                   Hbox.getChildren().addAll( Ok, canc);
                   pane.getChildren().addAll( fontBox, captionBox);
                    captionField.setEditable(true);
        Button selectHype = new Button ("Select Text");
        Button removeHype = new Button ("remove");
        HBox Box = new HBox();
        Box.getChildren().addAll(selectHype, removeHype);
        HBox Hbox2 = new HBox();
   
        Label link = new Label ("link"); 
        TextField area = new TextField();
          Hbox2.getChildren().addAll(link, area, Box);
	pane.getChildren().addAll(Hbox2, Hbox);
        
      total = captionField.getText();
     removeHype.setDisable(true);
        
        selectHype.setOnAction (e->{ 
            System.out.println("m");
            strings.add(captionField.getText());
               Label sample = new Label("Select Text: " +   captionField.getSelectedText());
               hype.add(captionField.getSelectedText());
        String linkSite = area.getText(); 
        removeHype.setDisable(false);
        Label site = new Label ("Link: " + linkSite);
        links.add(linkSite);
        HBox Vbox = new HBox();
        Vbox.getChildren().addAll(sample, site);
       
            Box.getChildren().addAll(Vbox);
            int x = captionField.getText().indexOf(captionField.getSelectedText());
            String m = captionField.getText().substring(0, x);
            String y = captionField.getText().substring(x ,captionField.getSelectedText().length()+x);
            String z = captionField.getText().substring(captionField.getSelectedText().length()+x);
             total = m + "<a href=\"" + linkSite + "\">" + y + "</a>" + z;
             System.out.print( " total " + total);
             captionField.setText(total);
            
         
        });
        removeHype.setOnAction(e->{
             strings.add(captionField.getText());
            //System.out.println( " string 1" + strings.get(strings.size()-1));
            strings.remove(strings.size()-1);
           // System.out.println(strings.get(strings.size()-1));
            captionField.setText(strings.get(strings.size()-1));
            links.remove(links.size()-1);
             hype.remove(hype.size()-1);
           Box.getChildren().remove(Box.getChildren().size()-1);
           if(links.size()<1){
                  removeHype.setDisable(true);
           }
           
        });
         font.getSelectionModel().selectedItemProperty()
            .addListener(e -> {
            x= (String)font.getSelectionModel().getSelectedItem(); 
            });
             Ok.setOnAction(e->{
                 if(total==null){
                     total = captionField.getText();
                 }
                   Page pagem = ui.getPage();
                   total = captionField.getText();
               strings.add(total);
                 pagem.addComponent(strings, null, "paragraph", null, strings.get(strings.size()-1), null, null, hype, links, null, x, null,null, null);
                stage.close(); 
             });     
                   canc.setOnAction(e->{
                stage.close(); 
             });     
                   stage.showAndWait();
                   

       }
    }
    
}
