/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

/**
 *
 * @author Scott
 */
public class EPortfolioGenerator extends Application{

    /**
     * @param args the command line arguments
     */
     EPortfolioFileManager fileManager = new EPortfolioFileManager();
    
    EPortfolioGeneratorView ui = new EPortfolioGeneratorView(fileManager);
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
       ui.startUI(primaryStage, "ePortfolio Generator");
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
