/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import javafx.geometry.Rectangle2D;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Screen;

import javafx.stage.Stage;
import javafx.stage.Window;
import javax.imageio.ImageIO;

/**
 *
 * @author Scott
 */
public class EPortfolioGeneratorView {
     Button newEPortfolioButton;
    Button loadEPortfolioButton;
    Button editComponent;
    Button saveEPortfolioButton;
    Button saveAsEPortfolioButton;
   Button exportEPortfolioButton;
    Button exitButton;
    Button titleButton;
    Button addComponent;
     Button removeComponent;
    Button bannerImage;
     Stage primaryStage;
    Scene primaryScene;
    static int i=0;
     FlowPane fileToolbarPane;
    ImageView imageSelectionView = new ImageView();
    int slideNumber=0;
      HBox workspace;
      VBox componentsEditorPane;
      ScrollPane componentsEditorScrollPane;
     VBox PageEditToolbar;
        Button addPageButton;
    Button removePageButton;
    Tab site = new Tab("Site Viewer");
    Button movePageUpButton;
    private FileController fileController;
    EPortfolioEditController editController;
    Button movePageDownButton;
    TextField nameField= new TextField();
    TabPane workSpaceToolbar;
    Tab PageEditor;
    Tab SiteViewer;
    String bImage="";
    int x;
    TabPane pagesEditorPane = new TabPane();
    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane EPortPane;
    EPortfolioModel EPortfolio;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    EPortfolioFileManager fileManager;
   public EPortfolioGeneratorView(EPortfolioFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	pagesEditorPane = new TabPane();
	// MAKE THE DATA MANAGING MODEL
	EPortfolio = new EPortfolioModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	//errorHandler = new ErrorHandler(this);
    } 
    public EPortfolioModel getEPortfolio() {
	return EPortfolio;
    }
    public Page getPage(){
        return  (Page) pagesEditorPane.getSelectionModel().getSelectedItem().getContent();
    }
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	
	String imagePath = "file:" + "images/icons/" + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
       
	return button;
    }
     public Stage getWindow() {
        
	return primaryStage;
    }
     private void initEventHandlers() {
        fileController = new FileController(this, fileManager);
	newEPortfolioButton.setOnAction(e -> {
            x=0;
	    fileController.handleNewEPortfolioRequest();
            addPageButton.setDisable(false);
            // moveSlideUpButton.setDisable(true);
            // moveSlideDownButton.setDisable(true);
	});
        loadEPortfolioButton.setOnAction(e -> {
         fileController.handleNewEPortfolioRequest();
	    fileController.handleLoadEPortfolioRequest();
               
                String windowTitle = "Eportfolio";
             primaryStage.setTitle(windowTitle + "-" + EPortfolio.getTitle());
              nameField.setText(EPortfolio.getName().substring(0, EPortfolio.getName().length()- 12));
             // fileController.handleLoadSlideShowRequest();
             //moveSlideUpButton.setDisable(true);
             //moveSlideDownButton.setDisable(true);
	});
        nameField.setOnKeyReleased(e->{
           EPortfolio.setName(nameField.getText());
        });
        saveAsEPortfolioButton.setOnAction(e->{
            fileController.handleSaveAsEPortfolioRequest();
              VBox pane =new VBox();
 Scene scene = new Scene(pane); 
  // scene.getStylesheets().add(STYLE_SHEET_UI);
           Stage stage = new Stage();
    //       stage.setTitle(sentence);
           stage.setScene(scene);
           Label Sentence = new Label("File Saved!");
           TextField text = new TextField();
       pane.getChildren().addAll(Sentence);
           Button okButton = new Button("OK");
           pane.getChildren().add(okButton);
           pane.getStyleClass().add("background");
           okButton.setOnMouseClicked(k->{
               
               stage.close();
           });
           stage.showAndWait();
        });
        saveEPortfolioButton.setOnAction(e -> {
            if(x==0){
                fileController.handleSaveAsEPortfolioRequest();
                x++;
            }
            else{  fileController.handleSaveEPortfolioRequest();
            }
              VBox pane =new VBox();
 Scene scene = new Scene(pane); 
  // scene.getStylesheets().add(STYLE_SHEET_UI);
           Stage stage = new Stage();
    //       stage.setTitle(sentence);
           stage.setScene(scene);
           Label Sentence = new Label("File Saved!");
           TextField text = new TextField();
       pane.getChildren().addAll(Sentence);
           Button okButton = new Button("OK");
           pane.getChildren().add(okButton);
           pane.getStyleClass().add("background");
           okButton.setOnMouseClicked(k->{
               
               stage.close();
           });
           stage.showAndWait();
            //addPageButton.setDisable(false);
            // moveSlideUpButton.setDisable(true);
            // moveSlideDownButton.setDisable(true);
	});
        
        exportEPortfolioButton.setOnAction(e->{
         
            fileController.handleSaveEPortfolioRequest();
                VBox pane =new VBox();
 Scene scene = new Scene(pane); 
  // scene.getStylesheets().add(STYLE_SHEET_UI);
           Stage stage = new Stage();
    //       stage.setTitle(sentence);
           stage.setScene(scene);
           Label Sentence = new Label("Your ePortfolio has been exported!");
           TextField text = new TextField();
       pane.getChildren().addAll(Sentence);
           Button okButton = new Button("OK");
           pane.getChildren().add(okButton);
           pane.getStyleClass().add("background");
           okButton.setOnMouseClicked(k->{
              
               stage.close();
           });
           stage.showAndWait();
        });
        exitButton.setOnAction(e -> {
	    //fileController.handleExitRequest();
            primaryStage.close();
	});
        editController = new EPortfolioEditController(this);
        addPageButton.setOnAction(e -> {
            
            //fileController.markAsEdited();
	    editController.processAddPageRequest();
           // viewSlideShowButton.setDisable(false);
              //EPortfolio.setSelected(null);
            removePageButton.setDisable(false);
           //  moveSlideUpButton.setDisable(true);
           //  moveSlideDownButton.setDisable(true);
	});
       
          removePageButton.setOnAction(e -> {
              Page page = (Page) pagesEditorPane.getSelectionModel().getSelectedItem().getContent();
           pagesEditorPane.getTabs().remove(pagesEditorPane.getSelectionModel().getSelectedItem());
           
        EPortfolio.getPages().remove(page);
            //fileController.markAsEdited();
	   // editController.processRemovePageRequest();
           // viewSlideShowButton.setDisable(false);
              //EPortfolio.setSelected(null);
          
           //  moveSlideUpButton.setDisable(true);
           //  moveSlideDownButton.setDisable(true);
	});
          pagesEditorPane.getSelectionModel().selectedItemProperty().addListener(e -> {
//              if(pagesEditorPane.getSelectionModel().getSelectedItem().getContent()!=null){
              Page page = (Page) pagesEditorPane.getSelectionModel().getSelectedItem().getContent();
              
        reloadPagePane(page);
              
    });
          
          }
      public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	EPortPane.setCenter(workSpaceToolbar);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	//saveEPortfolioButton.setDisable(saved);
//        if(EPortfolio.getPages().size()==0){
//	viewSlideShowButton.setDisable(true);
//        }
//        else{
//          viewSlideShowButton.setDisable(false);  
//        }
	// AND THE SLIDESHOW EDIT TOOLBAR
	//addSlideButton.setDisable(false);
        //TitleField.setText(slideShow.getTitle());
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
	String stageTitle = "Temp";
        primaryStage.setTitle(stageTitle + "-" + EPortfolio.getTitle());
    }
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
        //primaryStage.setTitle(slideShow.getTitle());
	initWindow(windowTitle);
    }
    public String getTitle(){
        return "test";
    }
        private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        fileToolbarPane.getStyleClass().add("horizontal");
        //fileToolbarPane.setHgap(10);
        //  fileToolbarPane.setVgap(10);
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	
	newEPortfolioButton = initChildButton(fileToolbarPane, "New.png",	"New Eportfolio ",	    "horizontal_toolbar", false);
	loadEPortfolioButton = initChildButton(fileToolbarPane, "Load.png",	"Load Eportfolio",    "horizontal_toolbar", false);
        nameField = new TextField();
       
	saveEPortfolioButton = initChildButton(fileToolbarPane, "SaveAs.png",	"Save ePortfolio",    "horizontal_toolbar", false);
        saveAsEPortfolioButton = initChildButton(fileToolbarPane, "Save.png",	"Save  As ePortfolio",    "horizontal_toolbar", false);
	exportEPortfolioButton = initChildButton(fileToolbarPane, "View.png",	"Export ePortfolio",    "horizontal_toolbar", false);
	exitButton = initChildButton(fileToolbarPane, "Exit.png", "Exit ePortfolio", "horizontal_toolbar", false);
         Label label = new Label ("Student Name: ");
        HBox Hbox = new HBox();
        Hbox.getChildren().addAll(label, nameField);
        fileToolbarPane.getChildren().add(Hbox);
       // TitleField = new TextField(EPortfolio.getTitle());
        
       // TitleField.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
	String title = "Default Title";
        //titleButton = new Button(title);
        //titleButton.getStyleClass().add("font");
       // titleButton.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON);
       // fileToolbarPane.getChildren().add(TitleField);
       // fileToolbarPane.getChildren().add(titleButton);
      //VBox VBoxTitle=new VBox();
    // VBoxTitle.addCaptionLabel()
       
    }
     private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
      
            primaryStage.setTitle(windowTitle);
        
	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	EPortPane = new BorderPane();
        EPortPane.getStyleClass().add("background");
	EPortPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(EPortPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add("/eportfoliogenerator/EPortfolioGeneratorStyle.css");

	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
     
    
      private void initWorkspace() {
          
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
	workSpaceToolbar = new TabPane();
      File htmlDest = new File("");
        File html = new File( "./public_html/index/index.html");
        Tab work = new Tab("Page Editor");
        work.setStyle("");
        work.setContent(workspace);
//        nameField.setOnKeyReleased(e->{
//            EPortfolio.setName(nameField.getText());
//           
//int i=0;
//while(i<EPortfolio.getPages().size()){
//    EPortfolio.getPages().get(i).setStudentName(nameField.getText());
//    reloadEPortfolioPane(EPortfolio);
//   // EPortfolio.getPages().nameField.setText(nameField.getText());
//    i++;
//}
//        });
//        
        workSpaceToolbar.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        workSpaceToolbar.getSelectionModel().selectedItemProperty().addListener(e -> {
            fileController.handleSiteViewEPortfolioRequest();
           handleSiteViewer(); 
        });
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        workSpaceToolbar.getTabs().add(work);
         workSpaceToolbar.getTabs().add(site);
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	PageEditToolbar = new VBox();
        //PageEditToolbar.setSpacing(100);
       //PageEditToolbar.setAlignment(Pos.CENTER);
	PageEditToolbar.getStyleClass().add("EPortfolio_edit");
         addPageButton = this.initChildButton(PageEditToolbar,		"Add.png",	    "Add Page",	    "vertical_toolbar",  true);
       removePageButton = this.initChildButton(PageEditToolbar,		"Remove.png",	    "Remove Page",	    "vertical_toolbar",  true);
      // movePageUpButton = this.initChildButton(PageEditToolbar,		"MoveUp.png",	    "Move Page Up",	    "vertical_toolbar",  true);
	// movePageDownButton = this.initChildButton(PageEditToolbar,		"MoveDown.png",	    "Move Page Down",	    "vertical_toolbar",  true);
        componentsEditorPane = new VBox();
	componentsEditorScrollPane = new ScrollPane(componentsEditorPane);
        
       pagesEditorPane.setSide(Side.TOP);
       pagesEditorPane.setStyle("-fx-background-color: violet");
   VBox Vbox = new VBox();
	  Vbox.getChildren().addAll(pagesEditorPane,  componentsEditorScrollPane);
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().addAll(PageEditToolbar);
        
            workspace.getChildren().addAll(Vbox);
    
}
      public void handleSiteViewer(){
          System.out.println("jkhkhkh");
          WebView browser = new WebView();
         WebEngine webEngine = browser.getEngine(); 
        int i=0;
        String index="";
        File htm = new File("");
        File htmlDest = new File("");
        while(i<EPortfolio.getPages().size()){
              System.out.println(i);
        new File("./Sites/" + EPortfolio.getTitle()).mkdirs();
        
        index ="index";
           if(i>0){
        index+=("" + (i+1) + "");
    }
           new File("./Sites/" + EPortfolio.getTitle()+ "/" + index).mkdirs();
      htm = new File( "./HTML5Application/public_html/index.html");
 
       htmlDest = new File( "./Sites/" + EPortfolio.getTitle()+ "/" + index + "/" + index + ".html");
try{
Files.copy(htm.toPath(), htmlDest.toPath());
} catch(IOException e){
    
}
 File css =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getLayout() + ".css");
      File cssDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getLayout() + ".css");
try{
Files.copy(css.toPath(), cssDest.toPath());
} catch(IOException e){
    
}
 File color =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getColor() + ".css");
      File colorDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getColor() + ".css");
try{
Files.copy(color.toPath(), colorDest.toPath());
} catch(IOException e){
    
}
 File font =  new File("./HTML5Application/public_html/" +EPortfolio.getPages().get(i).getFont() + ".css");
      File fontDest = new File( "./Sites/" + EPortfolio.getTitle() + "/" + index + "/" + EPortfolio.getPages().get(i).getFont() + ".css");
try{
Files.copy(font.toPath(), fontDest.toPath());
} catch(IOException e){
    
}
    File js =  new File("./HTML5Application/public_html/ePortfolio.js");

        
           File jsDest = new File( "./Sites/" + EPortfolio.getTitle() + "/ePortfolio.js");
   
     
       
try{
Files.copy(js.toPath(), jsDest.toPath());
} catch(IOException e){
    
}
File json  = new File("./data/eportfolios/" + EPortfolio.getTitle() + ".json");
 File jsonDest = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index + "/test" + ".json");
try{
Files.copy(json.toPath(), jsonDest.toPath());
} catch(IOException e){
    
}

//Delete if tempFile exists
File fileTemp = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index + "/test" + ".json");
if (fileTemp.exists()){
    System.out.println("delete");
    fileTemp.delete();
}
 json  = new File("./data/eportfolios/" + EPortfolio.getTitle() + ".json");
  jsonDest = new File( "./Sites/" + EPortfolio.getTitle() +"/" + index+  "/test" + ".json");
try{
Files.copy(json.toPath(), jsonDest.toPath());
} catch(IOException e){
    
}
       File file = new File(bImage);
       
          BufferedImage image = null;
        try {
          
            
            image = ImageIO.read(file);
            
            ImageIO.write(image, "jpg",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            ImageIO.write(image, "gif",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            ImageIO.write(image, "png",new File("./Sites/" + EPortfolio.getTitle()+"/" + index + "/BannerImage.jpg"));
            
        } catch (IOException e) {
        	
        }
           
        
        i++;}
      
        
         
        webEngine.load(htmlDest.toURI().toString());
        
        
        site.setStyle("");
        site.setContent(browser);
    
    
    
      }
      public void addPageTab(Tab tab) {
          System.out.println("ame 3" + tab.getText());
       Page p = (Page) tab.getContent();
        pagesEditorPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
       addComponent = new Button();
     
       String imagePath = "file:" + "images/icons/" + "addC.png";
	Image buttonImage = new Image(imagePath);
	
	addComponent.getStyleClass().add("vertical_toolbar");
	
	addComponent.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip("Add Component");
	addComponent.setTooltip(buttonTooltip);
	editComponent= new Button();
        editComponent.getStyleClass().add("vertical_toolbar");
      
       String path2 = "file:" + "images/icons/" + "editC.png";
       Image buttonImage2 = new Image(path2);
       editComponent.setGraphic(new ImageView(buttonImage2));
	Tooltip buttonTooltip2 = new Tooltip("Edit Component");
        
	editComponent.setTooltip(buttonTooltip2);
         bannerImage = new Button ();
          bannerImage.setDisable(true);
       String path3 = "file:" + "images/icons/" + "addBan.png";
       Image buttonImage3 = new Image(path3);
       bannerImage.setGraphic(new ImageView(buttonImage3));
	Tooltip buttonTooltip3 = new Tooltip("Add Banner Image");
        bannerImage.getStyleClass().add("vertical_toolbar");
	bannerImage.setTooltip(buttonTooltip3);
          removeComponent= new Button();
          String path4 = "file:" + "images/icons/" + "removeC.png";
       Image buttonImage4 = new Image(path4);
       removeComponent.setGraphic(new ImageView(buttonImage4));
	Tooltip buttonTooltip4 = new Tooltip("Remove Component");
     removeComponent.setTooltip(buttonTooltip4);
     removeComponent.getStyleClass().add("vertical_toolbar");
          ChoiceBox layout = new ChoiceBox();
 layout.getItems().addAll("Select Layout ", "Grace", "Yonder", "Dropdown", "Horizon", "Escape");
 layout.getSelectionModel().selectFirst();
 p.setLayout("default");
 layout.getSelectionModel().selectedItemProperty()
            .addListener(e -> {
            System.out.println((String)layout.getSelectionModel().getSelectedItem());  
p.setLayout((String)layout.getSelectionModel().getSelectedItem());
System.out.println(p.getLayout());
 if (layout.getSelectionModel().getSelectedItem().equals("Select Layout ") || layout.getSelectionModel().getSelectedItem().equals("Yonder") || layout.getSelectionModel().getSelectedItem().equals("Grace")|| layout.getSelectionModel().getSelectedItem().equals("Dropdown")){
     bannerImage.setDisable(true);
     p.setBannerImage(null);
 }
 else{
     bannerImage.setDisable(false);
 }
 });
 
     ChoiceBox color = new ChoiceBox();
 color.getItems().addAll("Select Color   ", "Purple Escape", "Ocean", "Teal Tide", "Silver Cloud", "SBU Pride");
 color.getSelectionModel().selectFirst();
 p.setColor("");
 color.getSelectionModel().selectedItemProperty()
            .addListener(e -> {
               p.setColor((String)color.getSelectionModel().getSelectedItem()); 
            });
     ChoiceBox font = new ChoiceBox();
 font.getItems().addAll("Select Font  ", "Open Sans", "Montserrat", "Lora", "Ubuntu", "Oxygen");
 font.getSelectionModel().selectFirst();
 p.setFont("");
font.getSelectionModel().selectedItemProperty()
            .addListener(e -> {
             p.setFont((String)font.getSelectionModel().getSelectedItem()); 
            });



 Label pageName  = new Label("Page Title:");
TextField pageField = new TextField (p.getTitle());
pageField.setOnKeyReleased(e->{
    
    tab.setText(pageField.getText());
    Page page = (Page) tab.getContent();
    page.setTitle(pageField.getText());
});
HBox pageBox = new HBox();
pageBox.getChildren().addAll(pageName, pageField);
pageBox.setSpacing(12);
Label footer  = new Label("Footer: ");
TextField footerField = new TextField (p.getFooter());
p.setFooter("");
footerField.setOnKeyReleased(e->{
p.setFooter(footerField.getText());
});
HBox footerBox = new HBox();
footerBox.getChildren().addAll(footer, footerField);
footerBox.setSpacing(20);

       HBox Hbox = new HBox();
       Hbox.setSpacing(5);
       Hbox.getChildren().addAll(addComponent, removeComponent, editComponent, bannerImage, layout, color, font, pageBox, footerBox);
       Page page = (Page) tab.getContent();
       page.getChildren().add(Hbox);
       //page.getChildren().add(componentsEditorScrollPane);
               pagesEditorPane.getTabs().add(tab);
              // pagesEditorPane.getSelectionModel().setSelectedItem(tab);
               tabHandlers(tab);
              
           // slideEditor.setBackground(background);
            //reloadEPortfolioPane(EPortfolio);
	}
      public void tabHandlers(Tab tab){
         
          removeComponent.setOnAction(e->{
              Page page = (Page) tab.getContent();
              page.removeComponent();
          });
           editComponent.setOnAction(e->{
              Page page = (Page) tab.getContent();
              page.editComponent();
          });
             
           addComponent.setOnAction(e -> {
          Alert compType = new Alert(AlertType.CONFIRMATION);
compType.setTitle("Component Options");
compType.setHeaderText("Choose your component type");


ButtonType text = new ButtonType("text");
ButtonType video = new ButtonType("video");
ButtonType image = new ButtonType("image");
ButtonType slideshow = new ButtonType("slideshow");
ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

compType.getButtonTypes().setAll(text, video, image, slideshow, cancel);

               Optional<ButtonType> result = compType.showAndWait();
               if (result.get() == text) {
Alert textType = new Alert(AlertType.CONFIRMATION);
textType.setTitle("Text Options");
textType.setHeaderText("Choose your text type");


ButtonType paragraph = new ButtonType("paragraph");
ButtonType header = new ButtonType("header");
ButtonType list = new ButtonType("list");
ButtonType canc = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
textType.getButtonTypes().setAll(header, list, paragraph, cancel);

Optional<ButtonType> answer = textType.showAndWait();
                if(answer.get() == paragraph){
                    ComponentProcess vid =  new ComponentProcess("paragraph", this);
                }
                 if(answer.get() == list){
                    ComponentProcess vid =  new ComponentProcess("list", this);
                }
                 if(answer.get() == header){
                    ComponentProcess vid =  new ComponentProcess("header", this);
                }
               } else if (result.get() == video) {
                    ComponentProcess vid = new ComponentProcess("video", this);
    
               } else if (result.get() == image) {
                ComponentProcess vid = new ComponentProcess("image", this);

               } 
                else if (result.get() == slideshow) {
                ComponentProcess vid = new ComponentProcess("slideshow", this);

               }
               else {

               }
           });
           bannerImage.setOnAction(e ->{
                 
               FileChooser imageFileChooser = new FileChooser();
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
        File file = imageFileChooser.showOpenDialog(null);
          String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
            bImage = path+ fileName;
         Page page = (Page) tab.getContent();
         page.setBannerImage(bImage);
//	    slideToEdit.setImage(path, fileName);
//	    view.updateSlideImage();
	// SET THE STARTING DIRECTORY

	
	// LET'S OPEN THE FILE CHOOSER
	
           });
      }
      public void reloadEPortfolioPane(EPortfolioModel EPortfolio) {
          int size =  pagesEditorPane.getTabs().size();
          
          if(size>0){
                  Tab tab=   pagesEditorPane.getSelectionModel().getSelectedItem();
     Page page =(Page) tab.getContent();
     page.clear();
     reloadPagePane(page);
          }
       
         pagesEditorPane.getTabs().remove(0,size);
      }
       public void reloadPagePane(Page PageToLoad) {
        int slideNum=-1;
	componentsEditorPane.getChildren().clear();
       
        // slideShow.setSelectedSlide(null);
     
     Tab tab=   pagesEditorPane.getSelectionModel().getSelectedItem();
     Page page =(Page) tab.getContent();
	for (Component component : page.getComponents()) {
//       
//      
	    ComponentEditView componentEditor = new ComponentEditView(component);
        component.setComponentEdit(componentEditor);
//            
//            //Background background = slideEditor.getBackground();
//            //reloadSlideShowPane(slideShowToLoad);
////            componentEditor.captionTextField.setOnKeyPressed(e->{
////                fileController.markAsEdited();
////            });
//           //  ImageSelectionController imageController;
//            
//           // imageController = new ImageSelectionController();
//          componentEditor.imageSelectionView.setOnMousePressed(e->{
////                fileController.markAsEdited();
////                
////	
////            
////	    imageController.processSelectImage(slide, slideEditor);
 //          component.getComponentEdit().getStyleClass().remove("component_edit_view_select"); 
 ///           component.getComponentEdit().getStyleClass().add("component_edit_view"); 
////               moveSlideDownButton.setDisable(true);
////                moveSlideUpButton.setDisable(true);
////                removeSlideButton.setDisable(true);
  //          });
           componentEditor.setOnMouseClicked(e->{
//         
//                
////                moveSlideDownButton.setDisable(true);
////                moveSlideUpButton.setDisable(true);
//              //handleSelect(slideShowToLoad, slide);  
//               
//            
//              
//              
//           
       
//           // slideEditor.updateSlideImage();
 Page pagem = getPage();
 pagem.selectComponent();
 pagem.setSelectedComponent(component);
 handleComponentSelected();
//                //removeSlideButton.setDisable(false);
//                //
           });
//          
//         
//               
	    componentsEditorPane.getChildren().add(componentEditor);
//              
//           // slideEditor.setBackground(background);
//            
	}
    }
     public void removePageTab(Tab tab) {
      
              pagesEditorPane.getTabs().remove(tab);
              
           // slideEditor.setBackground(background);
            
	}

   public void handleComponentSelected(){
       Page page = getPage();
        Component component= page.getSelectedComponent();
         component.getComponentEdit().getStyleClass().remove("component_edit_view_select"); 
     component.getComponentEdit().getStyleClass().add("component_edit_view_select"); 
                
                 for(int i=0; i<page.getComponents().size(); i++){
                     if(!(component.equals(page.getComponents().get(i)))){
                  
                   page.getComponents().get(i).getComponentEdit().getStyleClass().remove("component_edit_view_select");
              page.getComponents().get(i).getComponentEdit().getStyleClass().add("component_edit_view"); 
               }
                }
   }

   

  

    
}