package eportfoliogenerator;




/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & Scott Carson (10982387)
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
  
	String DEFAULT_CAPTION = "default";
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	
	slideShow.addSlide("DefaultStartSlide.png", "./images/slide_show_images/", DEFAULT_CAPTION);
    }
    public void processRemoveSlideRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	
	slideShow.removeSlide();
    }
    public void processmoveSlideUpRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	
	slideShow.moveSlideUp();
    }
      public void processmoveSlideDownRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	
	slideShow.moveSlideDown();
    }
      public void processSelectRequest(){
          SlideShowModel slideShow = ui.getSlideShow();
	
        slideShow.selectSlide();
      }
}
